<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'monitoring'], function () {
    Route::get('/keuangan', 'MonitoringController@keuangan')->name('ui-monitoring-keuangan');

    Route::get('/periode', 'HomeController@periode')->name('ui-monitoring-periode');
});

Route::group(['prefix' => 'report'], function () {
    Route::get('/penggajian', 'ReportController@penggajian')->name('ui-report-penggajian');
    Route::get('/jurnalumum', 'ReportController@jurnalumum')->name('ui-report-jurnalumum');
    Route::get('/jurnalumum/detail', 'ReportController@detailjurnalumum')->name('ui-report-detailjurnalumum');
    Route::get('/bukubesar', 'ReportController@bukubesar')->name('ui-report-bukubesar');
    Route::get('/bukubesar/detail', 'ReportController@detailbukubesar')->name('ui-report-detailbukubesar');
    Route::get('/rugilaba', 'ReportController@rugilaba')->name('ui-report-rugilaba');
    Route::get('/rugilaba/detail/{id}', 'ReportController@detailrugilaba')->name('ui-report-rugilaba-detail');
});

Route::group(['prefix' => 'pegawai'], function () {
    // ui
    Route::get('', 'PegawaiController@index')->name('ui-pegawai');
    Route::get('/search', 'PegawaiController@search')->name('ui-pegawai-search');
    Route::get('/create', 'PegawaiController@create')->name('ui-pegawai-create');
    Route::get('/edit/{id}', 'PegawaiController@edit')->name('ui-pegawai-edit');

    // crud
    Route::post('/save', 'PegawaiController@save')->name('ui-pegawai-save');
    Route::post('/update', 'PegawaiController@update')->name('ui-pegawai-update');
    Route::post('/delete', 'PegawaiController@delete')->name('ui-pegawai-delete');
});

Route::group(['prefix' => 'pengguna'], function () {
    // ui
    Route::get('', 'PenggunaController@index')->name('ui-pengguna');
    Route::get('/create', 'PenggunaController@create')->name('ui-pengguna-create');
    Route::get('/edit/{id}', 'PenggunaController@edit')->name('ui-pengguna-edit');

    // crud
    Route::post('/save', 'PenggunaController@save')->name('ui-pengguna-save');
    Route::post('/update', 'PenggunaController@update')->name('ui-pengguna-update');
    Route::post('/delete', 'PenggunaController@delete')->name('ui-pengguna-delete');
});

Route::group(['prefix' => 'penggajian'], function () {
    // ui
    Route::get('', 'PenggajianController@index')->name('ui-penggajian');
    Route::get('/create', 'PenggajianController@create')->name('ui-penggajian-create');
    Route::get('/edit/{id}', 'PenggajianController@edit')->name('ui-penggajian-edit');

    // crud
    Route::post('/save', 'PenggajianController@save')->name('ui-penggajian-save');
    Route::post('/update', 'PenggajianController@update')->name('ui-penggajian-update');
    Route::post('/delete', 'PenggajianController@delete')->name('ui-penggajian-delete');
});

Route::group(['prefix' => 'pendapatan'], function () {
    // ui
    Route::get('', 'PendapatanController@index')->name('ui-pendapatan');
    Route::get('/create', 'PendapatanController@create')->name('ui-pendapatan-create');
    Route::get('/edit/{id}', 'PendapatanController@edit')->name('ui-pendapatan-edit');

    // crud
    Route::post('/save', 'PendapatanController@save')->name('ui-pendapatan-save');
    Route::post('/update', 'PendapatanController@update')->name('ui-pendapatan-update');
    Route::post('/delete', 'PendapatanController@delete')->name('ui-pendapatan-delete');
    Route::post('/approve', 'PendapatanController@approve')->name('ui-pendapatan-approve');
});

Route::group(['prefix' => 'periode-keuangan'], function () {
    // ui
    Route::get('', 'PeriodeKeuanganController@index')->name('ui-periode-keuangan');
    Route::get('/create', 'PeriodeKeuanganController@create')->name('ui-periode-keuangan-create');
    Route::get('/edit/{id}', 'PeriodeKeuanganController@edit')->name('ui-periode-keuangan-edit');

    // crud
    Route::post('/save', 'PeriodeKeuanganController@save')->name('ui-periode-keuangan-save');
    Route::post('/update', 'PeriodeKeuanganController@update')->name('ui-periode-keuangan-update');
    Route::post('/delete', 'PeriodeKeuanganController@delete')->name('ui-periode-keuangan-delete');
});


Route::group(['prefix' => 'jenis-keuangan'], function () {
    // ui
    Route::get('', 'JenisKeuanganController@index')->name('ui-jenis-keuangan');
    Route::get('/create', 'JenisKeuanganController@create')->name('ui-jenis-keuangan-create');
    Route::get('/edit/{id}', 'JenisKeuanganController@edit')->name('ui-jenis-keuangan-edit');

    // crud
    Route::post('/save', 'JenisKeuanganController@save')->name('ui-jenis-keuangan-save');
    Route::post('/update', 'JenisKeuanganController@update')->name('ui-jenis-keuangan-update');
    Route::post('/delete', 'JenisKeuanganController@delete')->name('ui-jenis-keuangan-delete');
});

Route::group(['prefix' => 'subjenis-keuangan'], function () {
    // ui
    Route::get('', 'SubJenisKeuanganController@index')->name('ui-subjenis-keuangan');
    Route::get('/create', 'SubJenisKeuanganController@create')->name('ui-subjenis-keuangan-create');
    Route::get('/edit/{id}', 'SubJenisKeuanganController@edit')->name('ui-subjenis-keuangan-edit');

    // crud
    Route::post('/save', 'SubJenisKeuanganController@save')->name('ui-subjenis-keuangan-save');
    Route::post('/update', 'SubJenisKeuanganController@update')->name('ui-subjenis-keuangan-update');
    Route::post('/delete', 'SubJenisKeuanganController@delete')->name('ui-subjenis-keuangan-delete');
});

Route::group(['prefix' => 'detail-keuangan'], function () {
    // ui
    Route::get('', 'DetailKeuanganController@index')->name('ui-detail-keuangan');
    Route::get('/create', 'DetailKeuanganController@create')->name('ui-detail-keuangan-create');
    Route::get('/edit/{id}', 'DetailKeuanganController@edit')->name('ui-detail-keuangan-edit');

    // crud
    Route::post('/save', 'DetailKeuanganController@save')->name('ui-detail-keuangan-save');
    Route::post('/update', 'DetailKeuanganController@update')->name('ui-detail-keuangan-update');
    Route::post('/delete', 'DetailKeuanganController@delete')->name('ui-detail-keuangan-delete');
    Route::post('/approve', 'DetailKeuanganController@approve')->name('ui-detail-keuangan-approve');
});

Route::group(['prefix' => 'kasbesar'], function () {
    // ui
    Route::get('', 'KasbesarController@index')->name('ui-kasbesar');
    Route::get('/create', 'KasbesarController@create')->name('ui-kasbesar-create');
    Route::get('/edit/{id}', 'KasbesarController@edit')->name('ui-kasbesar-edit');

    // crud
    Route::post('/save', 'KasbesarController@save')->name('ui-kasbesar-save');
    Route::post('/update', 'KasbesarController@update')->name('ui-kasbesar-update');
    Route::post('/delete', 'KasbesarController@delete')->name('ui-kasbesar-delete');
});