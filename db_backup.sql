-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: db_keuangan
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `detailkeuangan`
--

DROP TABLE IF EXISTS `detailkeuangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detailkeuangan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jumlah_keuangan` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `periodekeuangan_id` bigint(20) unsigned NOT NULL,
  `subjeniskeuangan_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detailkeuangan_periodekeuangan_id_foreign` (`periodekeuangan_id`),
  KEY `detailkeuangan_subjeniskeuangan_id_foreign` (`subjeniskeuangan_id`),
  CONSTRAINT `detailkeuangan_periodekeuangan_id_foreign` FOREIGN KEY (`periodekeuangan_id`) REFERENCES `periodekeuangan` (`id`),
  CONSTRAINT `detailkeuangan_subjeniskeuangan_id_foreign` FOREIGN KEY (`subjeniskeuangan_id`) REFERENCES `subjeniskeuangan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detailkeuangan`
--

LOCK TABLES `detailkeuangan` WRITE;
/*!40000 ALTER TABLE `detailkeuangan` DISABLE KEYS */;
INSERT INTO `detailkeuangan` VALUES (1,'5972000','1',1,1,'2020-07-26 04:37:30','2020-07-26 06:46:20'),(2,'9169843200','1',1,2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'1340400000','1',1,3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'90535000','1',1,4,'2020-07-26 04:37:30','2020-07-26 09:25:04'),(5,'393000','1',1,5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(6,'83797000','1',1,6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(7,'7290000','1',1,7,'2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `detailkeuangan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jabatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES (1,'OPERATOR EXCAVATOR','OPERATOR EXCAVATOR','2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'HSE','HSE','2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'DIREKTUR','DIREKTUR','2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'STAFF OPERASIONAL','STAFF OPERASIONAL','2020-07-26 04:37:30','2020-07-26 04:37:30'),(5,'OPERATOR WHEEL LOADER','OPERATOR WHEEL LOADER','2020-07-26 04:37:30','2020-07-26 04:37:30'),(6,'MANAGER PRODUKSI','MANAGER PRODUKSI','2020-07-26 04:37:30','2020-07-26 04:37:30'),(7,'SALES/MARKETING','SALES/MARKETING','2020-07-26 04:37:30','2020-07-26 04:37:30'),(8,'MANAGER OPERASIONAL','MANAGER OPERASIONAL','2020-07-26 04:37:30','2020-07-26 04:37:30'),(9,'SEKURITI','SEKURITI','2020-07-26 04:37:30','2020-07-26 04:37:30'),(10,'OPERATOR WHEEL LOADER','OPERATOR WHEEL LOADER','2020-07-26 04:37:30','2020-07-26 04:37:30'),(11,'OPERATOR EXCAVATOR','OPERATOR EXCAVATOR','2020-07-26 04:37:30','2020-07-26 04:37:30'),(12,'MANAGER FINANCE','MANAGER FINANCE','2020-07-26 04:37:30','2020-07-26 04:37:30'),(13,'STAFF FINANCE','STAFF FINANCE','2020-07-26 04:37:30','2020-07-26 04:37:30'),(14,'MEKANIK','MEKANIK','2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jeniskeuangan`
--

DROP TABLE IF EXISTS `jeniskeuangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jeniskeuangan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_keuangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jeniskeuangan`
--

LOCK TABLES `jeniskeuangan` WRITE;
/*!40000 ALTER TABLE `jeniskeuangan` DISABLE KEYS */;
INSERT INTO `jeniskeuangan` VALUES (1,'Biaya Peralatan','2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'Biaya HPP','2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'Biaya Penggajian','2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'Biaya Beban Perusahaan','2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `jeniskeuangan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kasbesar`
--

DROP TABLE IF EXISTS `kasbesar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kasbesar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kredit` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailkeuangan_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kasbesar_detailkeuangan_id_foreign` (`detailkeuangan_id`),
  CONSTRAINT `kasbesar_detailkeuangan_id_foreign` FOREIGN KEY (`detailkeuangan_id`) REFERENCES `detailkeuangan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kasbesar`
--

LOCK TABLES `kasbesar` WRITE;
/*!40000 ALTER TABLE `kasbesar` DISABLE KEYS */;
INSERT INTO `kasbesar` VALUES (16,'2020-06-29','Biaya peralatan pegawai','5972000','1200000','7172000',1,'2020-07-26 06:46:20','2020-07-26 06:46:20'),(17,'2020-07-29','Biaya perjalanan dinas ke majalengka','90535000','10000000','100535000',4,'2020-07-26 09:25:04','2020-07-26 09:25:04');
/*!40000 ALTER TABLE `kasbesar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2020_07_06_162809_create_jabatan_table',1),(3,'2020_07_06_162809_create_pegawai_table',1),(4,'2020_07_06_162841_create_pengguna_table',1),(5,'2020_07_06_172400_create_periodekeuangan_table',1),(6,'2020_07_06_172922_create_pendapatan_table',1),(7,'2020_07_06_172943_create_jeniskeuangan_table',1),(8,'2020_07_06_172959_create_subjeniskeuangan_table',1),(9,'2020_07_06_173009_create_detailkeuangan_table',1),(10,'2020_07_06_173021_create_kasbesar_table',1),(11,'2020_07_20_152435_create_penggajian_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawai` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pegawai_nip_unique` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pegawai`
--

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;
INSERT INTO `pegawai` VALUES (1,'0231500008','A. JAJAT SUDRAJAT','OPERATOR EXCAVATOR','2015-01-02','2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'0231700029','A. JAJADI ANDRIANI','HSE','2017-02-12','2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'0231200002','ASEP GUNAWAN NUGRAHA','DIREKTUR','2012-04-01','2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'0231600034','ASRI NUR AISYAH','STAFF OPERASIONAL','2016-10-08','2020-07-26 04:37:30','2020-07-26 04:37:30'),(5,'0231600012','BIMA SUCI','OPERATOR WHEEL LOADER','2016-06-08','2020-07-26 04:37:30','2020-07-26 04:37:30'),(6,'0231600020','EDI WAHYONO','MANAGER PRODUKSI','2016-02-18','2020-07-26 04:37:30','2020-07-26 04:37:30'),(7,'0231700033','GIAR HELMY MAULANA','SALES/MARKETING','2017-03-02','2020-07-26 04:37:30','2020-07-26 04:37:30'),(8,'0231600037','HANDRIAN ARYA WIJAYA','MANAGER OPERASIONAL','2016-12-21','2020-07-26 04:37:30','2020-07-26 04:37:30'),(9,'0231500006','MAHMUDI','SEKURITI','2015-06-02','2020-07-26 04:37:30','2020-07-26 04:37:30'),(10,'0231500009','MUHAMAD NASIR','OPERATOR WHEEL LOADER','2015-06-02','2020-07-26 04:37:30','2020-07-26 04:37:30'),(11,'0231700026','RASTO JAYA MONI WIRANTO','OPERATOR EXCAVATOR','2017-12-09','2020-07-26 04:37:30','2020-07-26 04:37:30'),(12,'0231500020','RISCHA APRIANI','MANAGER FINANCE','2015-02-18','2020-07-26 04:37:30','2020-07-26 04:37:30'),(13,'0231600010','SINTA NOVIANTI','STAFF FINANCE','2016-07-16','2020-07-26 04:37:30','2020-07-26 04:37:30'),(14,'0231700011','TAJUDIN','MEKANIK','2017-04-02','2020-07-26 04:37:30','2020-07-26 04:37:30'),(15,'SPL1111','ADMIN','ADMIN','2017-12-02','2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pendapatan`
--

DROP TABLE IF EXISTS `pendapatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendapatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_pendapatan` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `pengguna_id` bigint(20) unsigned NOT NULL,
  `periodekeuangan_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pendapatan_pengguna_id_foreign` (`pengguna_id`),
  KEY `pendapatan_periodekeuangan_id_foreign` (`periodekeuangan_id`),
  CONSTRAINT `pendapatan_pengguna_id_foreign` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`id`),
  CONSTRAINT `pendapatan_periodekeuangan_id_foreign` FOREIGN KEY (`periodekeuangan_id`) REFERENCES `periodekeuangan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendapatan`
--

LOCK TABLES `pendapatan` WRITE;
/*!40000 ALTER TABLE `pendapatan` DISABLE KEYS */;
INSERT INTO `pendapatan` VALUES (1,'2020-07-26','Penjualan Material Tanah','15000000','1',1,1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'2020-07-26','Sewa Kendaraan','18000000','1',1,1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'2020-07-26','Sewa Alat Berat','25000000','1',1,1,'2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `pendapatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penggajian`
--

DROP TABLE IF EXISTS `penggajian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `penggajian` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gaji` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tunjangankesehatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tunjanganhariraya` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `potongan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `pegawai_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `penggajian_pegawai_id_foreign` (`pegawai_id`),
  CONSTRAINT `penggajian_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penggajian`
--

LOCK TABLES `penggajian` WRITE;
/*!40000 ALTER TABLE `penggajian` DISABLE KEYS */;
INSERT INTO `penggajian` VALUES (15,'3000000','600000','0','0','2018-01-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(16,'14000000','1000000','0','0','2018-01-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(17,'18000000','1200000','0','0','2018-01-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(18,'6000000','600000','0','0','2018-01-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(19,'3000000','600000','0','0','2018-01-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(20,'11000000','1000000','0','0','2018-01-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(21,'3500000','600000','0','0','2018-01-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(22,'11000000','1000000','0','0','2018-01-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(23,'2500000','500000','0','0','2018-01-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(24,'3000000','600000','0','0','2018-01-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(25,'3000000','600000','0','0','2018-01-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(26,'13000000','1000000','0','0','2018-01-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(27,'6000000','600000','0','0','2018-01-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(28,'4000000','500000','0','0','2018-01-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(29,'3000000','600000','0','0','2018-02-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(30,'14000000','1000000','0','0','2018-02-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(31,'18000000','1200000','0','0','2018-02-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(32,'6000000','600000','0','0','2018-02-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(33,'3000000','600000','0','0','2018-02-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(34,'11000000','1000000','0','0','2018-02-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(35,'3500000','600000','0','0','2018-02-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(36,'11000000','1000000','0','0','2018-02-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(37,'2500000','500000','0','0','2018-02-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(38,'3000000','600000','0','0','2018-02-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(39,'3000000','600000','0','0','2018-02-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(40,'13000000','1000000','0','0','2018-02-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(41,'6000000','600000','0','0','2018-02-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(42,'4000000','500000','0','0','2018-02-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(43,'3000000','600000','0','0','2018-03-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(44,'14000000','1000000','0','0','2018-03-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(45,'18000000','1200000','0','0','2018-03-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(46,'6000000','600000','0','0','2018-03-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(47,'3000000','600000','0','0','2018-03-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(48,'11000000','1000000','0','0','2018-03-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(49,'3500000','600000','0','0','2018-03-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(50,'11000000','1000000','0','0','2018-03-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(51,'2500000','500000','0','0','2018-03-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(52,'3000000','600000','0','0','2018-03-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(53,'3000000','600000','0','0','2018-03-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(54,'13000000','1000000','0','0','2018-03-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(55,'6000000','600000','0','0','2018-03-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(56,'4000000','500000','0','0','2018-03-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(57,'3000000','600000','0','0','2018-04-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(58,'14000000','1000000','0','0','2018-04-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(59,'18000000','1200000','0','0','2018-04-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(60,'6000000','600000','0','0','2018-04-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(61,'3000000','600000','0','0','2018-04-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(62,'11000000','1000000','0','0','2018-04-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(63,'3500000','600000','0','0','2018-04-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(64,'11000000','1000000','0','0','2018-04-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(65,'2500000','500000','0','0','2018-04-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(66,'3000000','600000','0','0','2018-04-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(67,'3000000','600000','0','0','2018-04-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(68,'13000000','1000000','0','0','2018-04-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(69,'6000000','600000','0','0','2018-04-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(70,'4000000','500000','0','0','2018-04-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(71,'3000000','600000','0','0','2018-05-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(72,'14000000','1000000','0','0','2018-05-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(73,'18000000','1200000','0','0','2018-05-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(74,'6000000','600000','0','0','2018-05-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(75,'3000000','600000','0','0','2018-05-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(76,'11000000','1000000','0','0','2018-05-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(77,'3500000','600000','0','0','2018-05-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(78,'11000000','1000000','0','0','2018-05-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(79,'2500000','500000','0','0','2018-05-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(80,'3000000','600000','0','0','2018-05-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(81,'3000000','600000','0','0','2018-05-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(82,'13000000','1000000','0','0','2018-05-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(83,'6000000','600000','0','0','2018-05-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(84,'4000000','500000','0','0','2018-05-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(85,'3000000','600000','3000000','0','2018-06-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(86,'14000000','1000000','14000000','0','2018-06-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(87,'18000000','1200000','18000000','0','2018-06-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(88,'6000000','600000','6000000','0','2018-06-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(89,'3000000','600000','3000000','0','2018-06-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(90,'11000000','1000000','11000000','0','2018-06-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(91,'3500000','600000','3500000','0','2018-06-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(92,'11000000','1000000','11000000','0','2018-06-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(93,'2500000','500000','2500000','0','2018-06-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(94,'3000000','600000','3000000','0','2018-06-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(95,'3000000','600000','3000000','0','2018-06-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(96,'13000000','1000000','13000000','0','2018-06-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(97,'6000000','600000','6000000','0','2018-06-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(98,'4000000','500000','4000000','0','2018-06-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(99,'3000000','600000','0','0','2018-07-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(100,'14000000','1000000','0','0','2018-07-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(101,'18000000','1200000','0','0','2018-07-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(102,'6000000','600000','0','0','2018-07-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(103,'3000000','600000','0','0','2018-07-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(104,'11000000','1000000','0','0','2018-07-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(105,'3500000','600000','0','0','2018-07-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(106,'11000000','1000000','0','0','2018-07-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(107,'2500000','500000','0','0','2018-07-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(108,'3000000','600000','0','0','2018-07-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(109,'3000000','600000','0','0','2018-07-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(110,'13000000','1000000','0','0','2018-07-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(111,'6000000','600000','0','0','2018-07-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(112,'4000000','500000','0','0','2018-07-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(113,'3000000','600000','0','0','2018-08-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(114,'14000000','1000000','0','0','2018-08-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(115,'18000000','1200000','0','0','2018-08-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(116,'6000000','600000','0','0','2018-08-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(117,'3000000','600000','0','0','2018-08-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(118,'11000000','1000000','0','0','2018-08-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(119,'3500000','600000','0','0','2018-08-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(120,'11000000','1000000','0','0','2018-08-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(121,'2500000','500000','0','0','2018-08-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(122,'3000000','600000','0','0','2018-08-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(123,'3000000','600000','0','0','2018-08-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(124,'13000000','1000000','0','0','2018-08-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(125,'6000000','600000','0','0','2018-08-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(126,'4000000','500000','0','0','2018-08-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(127,'3000000','600000','0','0','2018-09-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(128,'14000000','1000000','0','0','2018-09-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(129,'18000000','1200000','0','0','2018-09-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(130,'6000000','600000','0','0','2018-09-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(131,'3000000','600000','0','0','2018-09-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(132,'11000000','1000000','0','0','2018-09-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(133,'3500000','600000','0','0','2018-09-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(134,'11000000','1000000','0','0','2018-09-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(135,'2500000','500000','0','0','2018-09-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(136,'3000000','600000','0','0','2018-09-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(137,'3000000','600000','0','0','2018-09-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(138,'13000000','1000000','0','0','2018-09-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(139,'6000000','600000','0','0','2018-09-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(140,'4000000','500000','0','0','2018-09-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(141,'3000000','600000','0','0','2018-10-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(142,'14000000','1000000','0','0','2018-10-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(143,'18000000','1200000','0','0','2018-10-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(144,'6000000','600000','0','0','2018-10-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(145,'3000000','600000','0','0','2018-10-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(146,'11000000','1000000','0','0','2018-10-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(147,'3500000','600000','0','0','2018-10-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(148,'11000000','1000000','0','0','2018-10-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(149,'2500000','500000','0','0','2018-10-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(150,'3000000','600000','0','0','2018-10-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(151,'3000000','600000','0','0','2018-10-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(152,'13000000','1000000','0','0','2018-10-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(153,'6000000','600000','0','0','2018-10-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(154,'4000000','500000','0','0','2018-10-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(155,'3000000','600000','0','0','2018-11-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(156,'14000000','1000000','0','0','2018-11-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(157,'18000000','1200000','0','0','2018-11-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(158,'6000000','600000','0','0','2018-11-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(159,'3000000','600000','0','0','2018-11-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(160,'11000000','1000000','0','0','2018-11-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(161,'3500000','600000','0','0','2018-11-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(162,'11000000','1000000','0','0','2018-11-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(163,'2500000','500000','0','0','2018-11-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(164,'3000000','600000','0','0','2018-11-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(165,'3000000','600000','0','0','2018-11-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(166,'13000000','1000000','0','0','2018-11-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(167,'6000000','600000','0','0','2018-11-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(168,'4000000','500000','0','0','2018-11-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(169,'3000000','600000','0','0','2018-12-25',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(170,'14000000','1000000','0','0','2018-12-25',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(171,'18000000','1200000','0','0','2018-12-25',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(172,'6000000','600000','0','0','2018-12-25',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(173,'3000000','600000','0','0','2018-12-25',5,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(174,'11000000','1000000','0','0','2018-12-25',6,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(175,'3500000','600000','0','0','2018-12-25',7,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(176,'11000000','1000000','0','0','2018-12-25',8,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(177,'2500000','500000','0','0','2018-12-25',9,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(178,'3000000','600000','0','0','2018-12-25',10,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(179,'3000000','600000','0','0','2018-12-25',11,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(180,'13000000','1000000','0','0','2018-12-25',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(181,'6000000','600000','0','0','2018-12-25',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(182,'4000000','500000','0','0','2018-12-25',14,'2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `penggajian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengguna`
--

DROP TABLE IF EXISTS `pengguna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengguna` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pegawai_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pengguna_username_unique` (`username`),
  UNIQUE KEY `pengguna_email_unique` (`email`),
  KEY `pengguna_pegawai_id_foreign` (`pegawai_id`),
  CONSTRAINT `pengguna_pegawai_id_foreign` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengguna`
--

LOCK TABLES `pengguna` WRITE;
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` VALUES (1,'ASEP GUNAWAN NUGRAHA','asep','asep@gmail.com',NULL,'$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2','pITq6JDkuGIVglbxHdTPtJcXkfvkRARGLNoYb5M9xhjisDt2Z57zunUiYysM',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'RISCHA APRIANI','rischa','rischa@gmail.com',NULL,'$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2','OQ7aicTf0rNdkXS8kRW5PAwLhgO86GF65C7LTFgbb1jf57Pk9ybxCLyMOv0H',12,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'SINTA NOVIANTI','sinta','sinta@gmail.com',NULL,'$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2','3H4gCiBG0INY9WBgM7oZhowJejAUUfpuJv1GHa005qGF2X9hQgoLPzbaxKbn',13,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'ADMIN','admin','admin@gmail.com',NULL,'$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2','Y2fV6nNJ2Mi0oBt3Pt4LV5e5Ocer8XSoz9FfjQ7g8GC6ANwUyNx8if9MAZ8s',15,'2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodekeuangan`
--

DROP TABLE IF EXISTS `periodekeuangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodekeuangan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `tahun` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodekeuangan`
--

LOCK TABLES `periodekeuangan` WRITE;
/*!40000 ALTER TABLE `periodekeuangan` DISABLE KEYS */;
INSERT INTO `periodekeuangan` VALUES (1,'2020-07-26','2018','1','2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'2020-07-26','2019','0','2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'2020-07-26','2020','0','2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `periodekeuangan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjeniskeuangan`
--

DROP TABLE IF EXISTS `subjeniskeuangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjeniskeuangan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subjenis_keuangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jeniskeuangan_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjeniskeuangan_jeniskeuangan_id_foreign` (`jeniskeuangan_id`),
  CONSTRAINT `subjeniskeuangan_jeniskeuangan_id_foreign` FOREIGN KEY (`jeniskeuangan_id`) REFERENCES `jeniskeuangan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjeniskeuangan`
--

LOCK TABLES `subjeniskeuangan` WRITE;
/*!40000 ALTER TABLE `subjeniskeuangan` DISABLE KEYS */;
INSERT INTO `subjeniskeuangan` VALUES (1,'Perlengkapan Pegawai',1,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(2,'HPP Sparepart Proyek',2,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(3,'Penggajian',3,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(4,'Perjalanan Dinas',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(5,'Fotocopy',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(6,'BBM, Parkir, Tol',4,'2020-07-26 04:37:30','2020-07-26 04:37:30'),(7,'Listrik, Telpon, Internet',4,'2020-07-26 04:37:30','2020-07-26 04:37:30');
/*!40000 ALTER TABLE `subjeniskeuangan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-09 12:40:00
