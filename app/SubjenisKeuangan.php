<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjenisKeuangan extends Model
{
    protected $table = 'subjeniskeuangan';

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'subjeniskeuangan.id',
            'subjeniskeuangan.subjenis_keuangan',
            'subjeniskeuangan.created_at',
            'subjeniskeuangan.updated_at',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->orderBy('subjeniskeuangan.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetById($queey, $id)
    {
        return $this
        ->select(
            'subjeniskeuangan.id',
            'subjeniskeuangan.subjenis_keuangan',
            'subjeniskeuangan.created_at',
            'subjeniskeuangan.updated_at',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('subjeniskeuangan.id', $id)
        ->first();
    }

    public function scopeGetByPeriode($queey, $periode)
    {
        return $this
        ->select(
            'subjeniskeuangan.id',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->join('detailkeuangan', 'detailkeuangan.id', '=', 'subjeniskeuangan.detailkeuangan_id')
        ->where('detailkeuangan.periodekeuangan_id', $periode)
        ->get();
    }
}
