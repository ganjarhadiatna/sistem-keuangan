<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetailKeuangan extends Model
{
    protected $table = 'detailkeuangan';

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'detailkeuangan.created_at',
            'detailkeuangan.updated_at',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->orderBy('detailkeuangan.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetAllApproved($queey, $limit)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'detailkeuangan.created_at',
            'detailkeuangan.updated_at',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('detailkeuangan.status', '1')
        ->orderBy('detailkeuangan.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetById($queey, $id)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'detailkeuangan.created_at',
            'detailkeuangan.updated_at',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('detailkeuangan.id', $id)
        ->where('detailkeuangan.status', '1')
        ->first();
    }

    public function scopeGetByPeriode($queey, $periode)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'detailkeuangan.created_at',
            'detailkeuangan.updated_at',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('detailkeuangan.periodekeuangan_id', $periode)
        ->where('detailkeuangan.status', '1')
        ->get();
    }

    public function scopeGetMonitoring($queey, $limit)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan',
            (DB::raw('(select sum(kasbesar.kredit) from kasbesar where kasbesar.detailkeuangan_id=detailkeuangan.id) as realisasi'))
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('detailkeuangan.status', '1')
        ->paginate($limit);
    }

    public function scopeGetMonitoringByPeriode($queey, $tahun)
    {
        return $this
        ->select(
            'detailkeuangan.id',
            'detailkeuangan.jumlah_keuangan',
            'detailkeuangan.status',
            'periodekeuangan.tanggal',
            'periodekeuangan.tahun',
            'subjeniskeuangan.id as id_subjenis_keuangan',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.id as id_jenis_keuangan',
            'jeniskeuangan.jenis_keuangan',
            (DB::raw('(select sum(kasbesar.kredit) from kasbesar where kasbesar.detailkeuangan_id=detailkeuangan.id) as realisasi')),
            (DB::raw('(select sum(detailkeuangan.jumlah_keuangan) from detailkeuangan join subjeniskeuangan on subjeniskeuangan.id=detailkeuangan.subjeniskeuangan_id where subjeniskeuangan.jeniskeuangan_id=jeniskeuangan.id) as jumlah_sub')),
            (DB::raw('(select sum(kasbesar.kredit) from kasbesar join detailkeuangan on detailkeuangan.id=kasbesar.detailkeuangan_id join subjeniskeuangan on subjeniskeuangan.id=detailkeuangan.subjeniskeuangan_id where subjeniskeuangan.jeniskeuangan_id=jeniskeuangan.id) as jumlah_realisasi')),
            (DB::raw('(select count(subjeniskeuangan.id) from subjeniskeuangan where subjeniskeuangan.jeniskeuangan_id=jeniskeuangan.id) as total_sub'))
        )
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('detailkeuangan.status', '1')
        ->where('periodekeuangan.id', $tahun)
        ->get();
    }
}
