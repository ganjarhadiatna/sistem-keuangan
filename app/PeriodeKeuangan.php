<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PeriodeKeuangan extends Model
{
    protected $table = 'periodekeuangan';

    public function scopeGetMonitoring($query, $periode)
    {
        return $this
        ->select(
            'periodekeuangan.id',
            'periodekeuangan.tahun',
            'detailkeuangan.jumlah_keuangan',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan',
            'pendapatan.jumlah_pendapatan'
            // (DB::raw('(select sum(pendapatan.jumlah_pendapatan) from pendapatan where pendapatan.periodekeuangan_id=periodekeuangan.id) as pendapatan ')),
            // (DB::raw('(select sum(kasbesar.kredit) from kasbesar join detailkeuangan on detailkeuangan.id=kasbesar.detailkeuangan_id where detailkeuangan.periodekeuangan_id=periodekeuangan.id) as pengeluaran'))
        )
        ->rightJoin('pendapatan', 'pendapatan.id', '=', 'periodekeuangan.id')
        ->rightJoin('detailkeuangan', 'detailkeuangan.id', '=', 'periodekeuangan.id')
        ->rightJoin('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->rightJoin('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->where('periodekeuangan.id', $periode)
        ->get();
    }

    public function scopeGetRugiLaba($query, $limit)
    {
        return $this
        ->select(
            'periodekeuangan.id',
            'periodekeuangan.tahun',
            (DB::raw('(select sum(pendapatan.jumlah_pendapatan) from pendapatan where pendapatan.periodekeuangan_id=periodekeuangan.id) as pendapatan ')),
            (DB::raw('(select sum(kasbesar.kredit) from kasbesar join detailkeuangan on detailkeuangan.id=kasbesar.detailkeuangan_id where detailkeuangan.periodekeuangan_id=periodekeuangan.id) as pengeluaran'))
        )
        ->paginate($limit);
    }
}
