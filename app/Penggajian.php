<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggajian extends Model
{
    protected $table = "penggajian";

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'penggajian.id',
            'penggajian.gaji',
            'penggajian.tunjangankesehatan',
            'penggajian.tunjanganhariraya',
            'penggajian.potongan',
            'penggajian.tanggal',
            'penggajian.created_at',
            'penggajian.updated_at',
            'pegawai.nip',
            'pegawai.nama',
            'pegawai.jabatan'
        )
        ->join('pegawai', 'pegawai.id', '=', 'penggajian.pegawai_id')
        ->orderBy('penggajian.id', 'asc')
        ->paginate($limit);
    }

    public function scopeGetById($queey, $id)
    {
        return $this
        ->select(
            'penggajian.id',
            'penggajian.gaji',
            'penggajian.tunjangankesehatan',
            'penggajian.tunjanganhariraya',
            'penggajian.potongan',
            'penggajian.tanggal',
            'penggajian.created_at',
            'penggajian.updated_at',
            'pegawai.nip',
            'pegawai.nama',
            'pegawai.jabatan'
        )
        ->join('pegawai', 'pegawai.id', '=', 'penggajian.pegawai_id')
        ->orderBy('penggajian.id', 'asc')
        ->where('penggajian.id', $id)
        ->first();
    }

    public function scopeGetByMonth($queey, $limit, $periode, $month)
    {
        return $this
        ->select(
            'penggajian.id',
            'penggajian.gaji',
            'penggajian.tunjangankesehatan',
            'penggajian.tunjanganhariraya',
            'penggajian.potongan',
            'penggajian.tanggal',
            'penggajian.created_at',
            'penggajian.updated_at',
            'pegawai.nip',
            'pegawai.nama',
            'pegawai.jabatan'
        )
        ->join('pegawai', 'pegawai.id', '=', 'penggajian.pegawai_id')
        ->orderBy('penggajian.id', 'asc')
        ->whereYear('penggajian.tanggal', $periode)
        ->whereMonth('penggajian.tanggal', $month)
        ->paginate($limit);
    }
}
