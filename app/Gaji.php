<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gaji extends Model
{
    protected $table = 'gaji';

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'gaji.id',
            'gaji.gaji',
            'gaji.created_at',
            'gaji.updated_at',
            'pegawai.nik',
            'pegawai.nama',
            'pegawai.jabatan'
        )
        ->join('pegawai', 'pegawai.id', '=', 'gaji.pegawai_id')
        ->orderBy('gaji.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetById($queey, $id)
    {
        return $this
        ->select(
            'gaji.id',
            'gaji.gaji',
            'gaji.created_at',
            'gaji.updated_at',
            'pegawai.nik',
            'pegawai.nama',
            'pegawai.jabatan'
        )
        ->join('pegawai', 'pegawai.id', '=', 'gaji.pegawai_id')
        ->orderBy('gaji.id', 'desc')
        ->where('gaji.id', $id)
        ->first();
    }
}
