<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kasbesar extends Model
{
    protected $table = 'kasbesar';

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'kasbesar.id',
            'kasbesar.tanggal',
            'kasbesar.keterangan',
            'kasbesar.debit',
            'kasbesar.kredit',
            'kasbesar.saldo',
            'kasbesar.created_at',
            'kasbesar.updated_at',
            'detailkeuangan.jumlah_keuangan',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('detailkeuangan', 'detailkeuangan.id', '=', 'kasbesar.detailkeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->orderBy('kasbesar.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetById($queey, $id)
    {
        return $this
        ->select(
            'kasbesar.id',
            'kasbesar.tanggal',
            'kasbesar.keterangan',
            'kasbesar.debit',
            'kasbesar.kredit',
            'kasbesar.saldo',
            'kasbesar.created_at',
            'kasbesar.updated_at',
            'detailkeuangan.jumlah_keuangan',
            'subjeniskeuangan.subjenis_keuangan',
            'jeniskeuangan.jenis_keuangan'
        )
        ->join('detailkeuangan', 'detailkeuangan.id', '=', 'kasbesar.detailkeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'kasbesar.subjeniskeuangan_id')
        ->where('kasbesar.id', $id)
        ->first();
    }

    public function scopeGetByPeriode($queey, $periode)
    {
        return $this
        ->select(
            'kasbesar.id',
            'kasbesar.tanggal',
            'kasbesar.keterangan',
            'kasbesar.debit',
            'kasbesar.kredit',
            'kasbesar.saldo',
            'detailkeuangan.jumlah_keuangan',
            'periodekeuangan.tahun'
        )
        ->join('detailkeuangan', 'detailkeuangan.id', '=', 'kasbesar.detailkeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->where('detailkeuangan.periodekeuangan_id', $periode)
        ->get();
    }

    public function scopeGetBukuBesar($queey, $periode, $bulan, $pos)
    {
        return $this
        ->select(
            'kasbesar.id',
            'kasbesar.tanggal',
            'kasbesar.keterangan',
            'kasbesar.debit',
            'kasbesar.kredit',
            'kasbesar.saldo',
            'detailkeuangan.jumlah_keuangan',
            'jeniskeuangan.jenis_keuangan',
            'subjeniskeuangan.subjenis_keuangan',
            'periodekeuangan.tahun'
        )
        ->join('detailkeuangan', 'detailkeuangan.id', '=', 'kasbesar.detailkeuangan_id')
        ->join('subjeniskeuangan', 'subjeniskeuangan.id', '=', 'detailkeuangan.subjeniskeuangan_id')
        ->join('jeniskeuangan', 'jeniskeuangan.id', '=', 'subjeniskeuangan.jeniskeuangan_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'detailkeuangan.periodekeuangan_id')
        ->whereMonth('kasbesar.tanggal', $bulan)
        ->where('detailkeuangan.periodekeuangan_id', $periode)
        ->where('subjeniskeuangan.jeniskeuangan_id', $pos)
        ->get();
    }
}
