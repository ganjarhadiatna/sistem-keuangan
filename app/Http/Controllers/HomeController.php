<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use App\Pegawai;
use App\PeriodeKeuangan;
use App\DetailKeuangan;
use App\JenisKeuangan;
use App\SubjenisKeuangan;
use App\Pendapatan;
use App\Kasbesar;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $admin = ['pengguna' => Pengguna::count(), 'pegawai' => Pegawai::count()];
        $data = ['periode' => PeriodeKeuangan::get()];
        return view('home', ['admin' => $admin, 'data' => $data]);
    }

    public function periode()
    {
        $tahun = $_GET['periode_keuangan'];
        $dtPR = PeriodeKeuangan::where('tahun', $tahun)->first();
        $dtPD = PeriodeKeuangan::where('tahun', ($tahun - 1))->first();

        if ($dtPD) {
            $pdID = $dtPD->id;
        } else {
            $pdID = 0;
        }

        $data = [
            'periode' => PeriodeKeuangan::get(),
            'rencana_keuangan' => DetailKeuangan::GetMonitoringByPeriode($dtPR->id),
            'pendapatan' => Pendapatan::GetByPeriode($pdID)
        ];
        // echo json_encode($data, JSON_PRETTY_PRINT);
        return view('home', ['data' => $data, 'periode' => $tahun, 'periode_before' => ($tahun - 1)]);
    }
}
