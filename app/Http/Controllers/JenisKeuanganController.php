<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisKeuangan;
use Auth;

class JenisKeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = JenisKeuangan::orderBy('id', 'desc')->paginate(5);
        return view('jeniskeuangan.index', ['data' => $data]);
    }

    public function create()
    {
        return view('jeniskeuangan.form');
    }

    public function edit($id)
    {
        $data = JenisKeuangan::where(['id' => $id])->first();
        return view('jeniskeuangan.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'jenis_keuangan' => 'required|min:0|max:255'
        ]);

        $data = [
            'jenis_keuangan' => $request->input('jenis_keuangan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = JenisKeuangan::insert($data);

        if ($service) 
        {
            return redirect('/jenis-keuangan');
        }
        else 
        {
            return redirect('/jenis-keuangan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'jenis_keuangan' => 'required|min:0|max:255'
        ]);

        $id = $request->input('id');

        $data = [
            'jenis_keuangan' => $request->input('jenis_keuangan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = JenisKeuangan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/jenis-keuangan');
        }
        else 
        {
            return redirect('/jenis-keuangan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = JenisKeuangan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/jenis-keuangan');
        }
        else 
        {
            return redirect('/jenis-keuangan');
        }
    }
}
