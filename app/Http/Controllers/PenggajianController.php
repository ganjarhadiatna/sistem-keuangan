<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Penggajian;

class PenggajianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Penggajian::GetAll(10);
        return view('penggajian.index', ['data' => $data]);
    }

    public function create()
    {
        $pegawai = Pegawai::get();
        return view('penggajian.form', ['pegawai' => $pegawai]);
    }

    public function edit($id)
    {
        $data = Penggajian::where(['id' => $id])->first();
        $pegawai = Pegawai::get();
        return view('penggajian.form', ['data' => $data, 'pegawai' => $pegawai]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'pegawai_id' => 'required|min:0|max:32|unique:penggajian',
            'gaji' => 'required|min:0|max:150',
            'tunjanganhariraya' => 'required|min:0|max:150',
            'tunjangankesehatan' => 'required|min:0|max:150',
            'potongan' => 'required|min:0|max:150',
            'tanggal' => 'required'
        ]);

        $data = [
            'pegawai_id' => $request->input('pegawai_id'),
            'gaji' => $request->input('gaji'),
            'tunjanganhariraya' => $request->input('tunjanganhariraya'),
            'tunjangankesehatan' => $request->input('tunjangankesehatan'),
            'potongan' => $request->input('potongan'),
            'tanggal' => $request->input('tanggal'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Penggajian::insert($data);

        if ($service) 
        {
            return redirect('/penggajian');
        }
        else 
        {
            return redirect('/penggajian/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'gaji' => 'required|min:0|max:150',
            'tunjanganhariraya' => 'required|min:0|max:150',
            'tunjangankesehatan' => 'required|min:0|max:150',
            'potongan' => 'required|min:0|max:150',
            'tanggal' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'gaji' => $request->input('gaji'),
            'tunjanganhariraya' => $request->input('tunjanganhariraya'),
            'tunjangankesehatan' => $request->input('tunjangankesehatan'),
            'potongan' => $request->input('potongan'),
            'tanggal' => $request->input('tanggal'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Penggajian::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/penggajian');
        }
        else 
        {
            return redirect('/penggajian/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Penggajian::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/penggajian');
        }
        else 
        {
            return redirect('/penggajian');
        }
    }
}
