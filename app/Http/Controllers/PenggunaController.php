<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Pengguna;
use App\Pegawai;

class PenggunaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pengguna::paginate(10);
        return view('pengguna.index', ['data' => $data]);
    }

    public function create()
    {
        $pegawai = Pegawai::get();
        return view('pengguna.form', ['pegawai' => $pegawai]);
    }

    public function edit($id)
    {
        $data = Pengguna::where(['id' => $id])->first();
        $pegawai = Pegawai::get();
        return view('pengguna.form', ['data' => $data, 'pegawai' => $pegawai]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|min:0|max:32|unique:pengguna',
            'nama' => 'required|min:0|max:150',
            'password' => 'required|min:8|max:150',
            'pegawai_id' => 'required'
        ]);

        $data = [
            'email' => $request->input('email'),
            'nama' => $request->input('nama'),
            'password' => Hash::make($request->input('password')),
            'pegawai_id' => $request->input('pegawai_id'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pengguna::insert($data);

        if ($service) 
        {
            return redirect('/pengguna');
        }
        else 
        {
            return redirect('/pengguna/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'email' => 'required|min:0|max:32',
            'nama' => 'required|min:0|max:150',
            'password' => 'required|min:8|max:150',
            'pegawai_id' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'email' => $request->input('email'),
            'nama' => $request->input('nama'),
            'password' => Hash::make($request->input('password')),
            'pegawai_id' => $request->input('pegawai_id'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pengguna::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pengguna');
        }
        else 
        {
            return redirect('/pengguna/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pengguna::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pengguna');
        }
        else 
        {
            return redirect('/pengguna');
        }
    }
}
