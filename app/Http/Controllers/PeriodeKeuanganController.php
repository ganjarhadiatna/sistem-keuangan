<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeriodeKeuangan;
use Auth;

class PeriodeKeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = PeriodeKeuangan::orderBy('id', 'desc')->paginate(5);
        return view('periodekeuangan.index', ['data' => $data]);
    }

    public function create()
    {
        return view('periodekeuangan.form');
    }

    public function edit($id)
    {
        $data = PeriodeKeuangan::where(['id' => $id])->first();
        return view('periodekeuangan.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'tanggal' => 'required|min:0',
            'tahun' => 'required|min:0',
            'status' => 'required'
        ]);

        $data = [
            'tanggal' => $request->input('tanggal'),
            'tahun' => $request->input('tahun'),
            'status' => $request->input('status'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = PeriodeKeuangan::insert($data);

        if ($service) 
        {
            return redirect('/periode-keuangan');
        }
        else 
        {
            return redirect('/periode-keuangan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'tanggal' => 'required|min:0',
            'tahun' => 'required|min:0',
            'status' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'tanggal' => $request->input('tanggal'),
            'tahun' => $request->input('tahun'),
            'status' => $request->input('status'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = PeriodeKeuangan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/periode-keuangan');
        }
        else 
        {
            return redirect('/periode-keuangan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = PeriodeKeuangan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/periode-keuangan');
        }
        else 
        {
            return redirect('/periode-keuangan');
        }
    }
}
