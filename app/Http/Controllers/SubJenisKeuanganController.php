<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubjenisKeuangan;
use App\JenisKeuangan;
use App\DetailKeuangan;
use Auth;

class SubJenisKeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = SubjenisKeuangan::GetAll(10);
        return view('subjeniskeuangan.index', ['data' => $data]);
    }

    public function create()
    {
        $jenis = JenisKeuangan::get();
        return view('subjeniskeuangan.form', ['jenis' => $jenis]);
    }

    public function edit($id)
    {
        $data = SubjenisKeuangan::where(['id' => $id])->first();
        $jenis = JenisKeuangan::get();
        return view('subjeniskeuangan.form', ['data' => $data, 'jenis' => $jenis]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'subjenis_keuangan' => 'required|min:0|max:255',
            'jeniskeuangan_id' => 'required'
        ]);

        $data = [
            'subjenis_keuangan' => $request->input('subjenis_keuangan'),
            'jeniskeuangan_id' => $request->input('jeniskeuangan_id'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = SubjenisKeuangan::insert($data);

        if ($service) 
        {
            return redirect('/subjenis-keuangan');
        }
        else 
        {
            return redirect('/subjenis-keuangan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'subjenis_keuangan' => 'required|min:0|max:255',
            'jeniskeuangan_id' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'subjenis_keuangan' => $request->input('subjenis_keuangan'),
            'jeniskeuangan_id' => $request->input('jeniskeuangan_id'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = SubjenisKeuangan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/subjenis-keuangan');
        }
        else 
        {
            return redirect('/subjenis-keuangan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = SubjenisKeuangan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/subjenis-keuangan');
        }
        else 
        {
            return redirect('/subjenis-keuangan');
        }
    }
}
