<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeriodeKeuangan;
use App\DetailKeuangan;
use App\JenisKeuangan;
use App\SubjenisKeuangan;
use Auth;

class DetailKeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = DetailKeuangan::GetAll(10);
        return view('detailkeuangan.index', ['data' => $data]);
    }

    public function create()
    {
        $periode = PeriodeKeuangan::where('status', '1')->get();
        $subjenis = SubjenisKeuangan::GetAll(100);
        return view('detailkeuangan.form', ['periode' => $periode, 'subjenis' => $subjenis]);
    }

    public function edit($id)
    {
        $data = DetailKeuangan::where(['id' => $id])->first();
        $periode = PeriodeKeuangan::where('status', '1')->get();
        $subjenis = SubjenisKeuangan::GetAll(100);
        return view('detailkeuangan.form', ['data' => $data, 'periode' => $periode, 'subjenis' => $subjenis]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'jumlah_keuangan' => 'required|min:0',
            'periodekeuangan_id' => 'required',
            'subjeniskeuangan_id' => 'required'
        ]);

        $data = [
            'jumlah_keuangan' => $request->input('jumlah_keuangan'),
            'periodekeuangan_id' => $request->input('periodekeuangan_id'),
            'subjeniskeuangan_id' => $request->input('subjeniskeuangan_id'),
            'status' => '0',
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = DetailKeuangan::insert($data);

        if ($service) 
        {
            return redirect('/detail-keuangan');
        }
        else 
        {
            return redirect('/detail-keuangan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'jumlah_keuangan' => 'required|min:0',
            'periodekeuangan_id' => 'required',
            'subjeniskeuangan_id' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'jumlah_keuangan' => $request->input('jumlah_keuangan'),
            'periodekeuangan_id' => $request->input('periodekeuangan_id'),
            'subjeniskeuangan_id' => $request->input('subjeniskeuangan_id'),
            'status' => '0',
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = DetailKeuangan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/detail-keuangan');
        }
        else 
        {
            return redirect('/detail-keuangan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = DetailKeuangan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/detail-keuangan');
        }
        else 
        {
            return redirect('/detail-keuangan');
        }
    }

    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $data = DetailKeuangan::where(['id' => $id])->first();
        $payload = [
            'status' => $data->status == '1' ? '0' : '1',
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = DetailKeuangan::where(['id' => $id])->update($payload);

        if ($service) 
        {
            return redirect('/detail-keuangan');
        }
        else 
        {
            return redirect('/detail-keuangan');
        }
    }
}
