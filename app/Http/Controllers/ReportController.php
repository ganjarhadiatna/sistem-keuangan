<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PeriodeKeuangan;
use App\Penggajian;
use App\Pendapatan;
use App\DetailKeuangan;
use App\JenisKeuangan;
use App\Kasbesar;

class ReportController extends Controller
{
    protected $month = [
        ['id' => '1', 'month' => 'Januari'],
        ['id' => '2', 'month' => 'Februari'],
        ['id' => '3', 'month' => 'Maret'],
        ['id' => '4', 'month' => 'April'],
        ['id' => '5', 'month' => 'Mei'],
        ['id' => '6', 'month' => 'Juni'],
        ['id' => '7', 'month' => 'Juli'],
        ['id' => '8', 'month' => 'Agustus'],
        ['id' => '9', 'month' => 'September'],
        ['id' => '10', 'month' => 'Oktober'],
        ['id' => '11', 'month' => 'November'],
        ['id' => '12', 'month' => 'Desember']
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function penggajian()
    {
        $tahun = isset($_GET['periode']) ? $_GET['periode'] : '';
        $bulan = isset($_GET['bulan']) ? $_GET['bulan'] : '';
        $data = isset($_GET['periode']) || isset($_GET['bulan']) ? Penggajian::GetByMonth(15, $_GET['periode'], $_GET['bulan']) : Penggajian::GetAll(0);
        $periode = PeriodeKeuangan::get();
        return view('report.penggajian', ['data' => $data, 'periode' => $periode, 'month' => $this->month, 'selected_month' => $bulan, 'selected_periode' => $tahun]);
    }

    public function bukubesar()
    {
        $periode = PeriodeKeuangan::get();
        $bulan = $this->month;
        $pos = JenisKeuangan::get();
        return view('report.bukubesar', ['periode' => $periode, 'bulan' => $bulan, 'pos' => $pos]);
    }

    public function detailbukubesar()
    {
        $tahun = $_GET['periode_keuangan'];
        $bulan_keuangan = $_GET['bulan'];
        $pos_keuangan = $_GET['pos_keuangan'];

        $pr = PeriodeKeuangan::where('tahun', $tahun)->first();
        $periode = PeriodeKeuangan::get();        
        $bulan = $this->month;
        $pos = JenisKeuangan::get();

        $data = Kasbesar::GetBukuBesar($pr->id, $bulan_keuangan, $pos_keuangan);

        return view('report.bukubesar', ['data' => $data, 'periode' => $periode, 'bulan' => $bulan, 'pos' => $pos, 'tahun' => $tahun, 'bulan_keuangan' => $bulan_keuangan, 'pos_keuangan' => $pos_keuangan]);
    }
    
    public function jurnalumum()
    {
        $periode = PeriodeKeuangan::get();
        return view('report.jurnalumum', ['periode' => $periode]);
    }

    public function detailjurnalumum()
    {
        $tahun = $_GET['periode_keuangan'];
        $pr = PeriodeKeuangan::where('tahun', $tahun)->first();
        $periode = PeriodeKeuangan::get();
        $data = Kasbesar::GetByPeriode($pr->id);
        return view('report.jurnalumum', ['periode' => $periode, 'tahun' => $tahun, 'data' => $data]);
    }
    
    public function rugilaba()
    {
        $data = PeriodeKeuangan::GetRugiLaba(10);
        return view('report.rugilaba', ['data' => $data]);
    }

    public function detailrugilaba($periode)
    {
        $data = PeriodeKeuangan::where('tahun', $periode)->first();
        $pendapatan = Pendapatan::GetByPeriode($data->id);
        $keuangan = Kasbesar::GetByPeriode($data->id);
        return view('report.rugilabadetail', ['pendapatan' => $pendapatan, 'keuangan' => $keuangan, 'periode' => $periode]);
    }
}
