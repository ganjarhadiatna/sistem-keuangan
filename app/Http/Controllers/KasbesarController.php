<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Pendapatan;
use App\JenisKeuangan;
use App\SubjenisKeuangan;
use App\DetailKeuangan;
use App\Kasbesar;

class KasbesarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Kasbesar::GetAll(5);
        return view('kasbesar.index', ['data' => $data]);
    }

    public function create()
    {
        $pendapatan = Pendapatan::get();
        $detailkeuangan = DetailKeuangan::GetAllApproved(100);
        return view('kasbesar.form', ['pendapatan' => $pendapatan, 'detailkeuangan' => $detailkeuangan]);
    }

    public function edit($id)
    {
        $data = Kasbesar::where(['id' => $id])->first();
        $pendapatan = Pendapatan::get();
        $detailkeuangan = DetailKeuangan::GetAllApproved(100);
        return view('kasbesar.form', ['data' => $data, 'pendapatan' => $pendapatan, 'detailkeuangan' => $detailkeuangan]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'kredit' => 'required|min:0|max:150',
            'keterangan' => 'required|min:0|max:150',
            'detailkeuangan_id' => 'required',
            'tanggal' => 'required'
        ]);

        $detailkeuangan_id = $request->input('detailkeuangan_id');
        $payload = DetailKeuangan::where('id', $detailkeuangan_id)->first();
        $jumlah_keuangan = $payload->jumlah_keuangan - $request->input('kredit');

        $data = [
            'debit' => $jumlah_keuangan,
            'kredit' => $request->input('kredit'),
            'saldo' => $payload->jumlah_keuangan,
            'keterangan' => $request->input('keterangan'),
            'detailkeuangan_id' => $request->input('detailkeuangan_id'),
            'tanggal' => $request->input('tanggal'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Kasbesar::insert($data);
        $service2 = DetailKeuangan::where('id', $detailkeuangan_id)->update(['jumlah_keuangan' => $jumlah_keuangan]);

        if ($service && $service2) 
        {
            return redirect('/kasbesar');
        }
        else 
        {
            return redirect('/kasbesar/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'keterangan' => 'required|min:0|max:150',
            'tanggal' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'keterangan' => $request->input('keterangan'),
            'tanggal' => $request->input('tanggal'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Kasbesar::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/kasbesar');
        }
        else 
        {
            return redirect('/kasbesar/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');
        $payload = Kasbesar::where('id', $id)->first();
        $payload2 = DetailKeuangan::where('id', $payload->detailkeuangan_id)->first();
        $jumlah_keuangan = $payload->kredit + $payload2->jumlah_keuangan;

        $service = Kasbesar::where(['id' => $id])->delete();
        $service2 = DetailKeuangan::where('id', $payload2->id)->update(['jumlah_keuangan' => $jumlah_keuangan]);

        if ($service && $service2) 
        {
            return redirect('/kasbesar');
        }
        else 
        {
            return redirect('/kasbesar');
        }
    }
}
