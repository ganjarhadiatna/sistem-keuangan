<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use App\Pegawai;
use App\PeriodeKeuangan;
use App\DetailKeuangan;
use App\JenisKeuangan;
use App\SubjenisKeuangan;
use App\Pendapatan;
use App\Kasbesar;

class MonitoringController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function keuangan()
    {
        $data = DetailKeuangan::GetMonitoring(10);
        return view('monitoring.keuangan', ['data' => $data]);
    }
}
