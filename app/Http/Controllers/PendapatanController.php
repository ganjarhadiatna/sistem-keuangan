<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pendapatan;
use App\Pegawai;
use App\PeriodeKeuangan;
use Auth;

class PendapatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pendapatan::GetAll(5);
        return view('pendapatan.index', ['data' => $data]);
    }

    public function create()
    {
        $pegawai = Pegawai::get();
        $periode = PeriodeKeuangan::where('status', '1')->get();
        return view('pendapatan.form', ['pegawai' => $pegawai, 'periode' => $periode]);
    }

    public function edit($id)
    {
        $data = Pendapatan::where(['id' => $id])->first();
        $pegawai = Pegawai::get();
        $periode = PeriodeKeuangan::where('status', '1')->get();
        return view('pendapatan.form', ['data' => $data, 'pegawai' => $pegawai, 'periode' => $periode]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'jumlah_pendapatan' => 'required|min:0|max:32',
            'keterangan' => 'required|min:0|max:255',
            'periodekeuangan_id' => 'required',
            'tanggal' => 'required'
        ]);

        $data = [
            'jumlah_pendapatan' => $request->input('jumlah_pendapatan'),
            'keterangan' => $request->input('keterangan'),
            'tanggal' => $request->input('tanggal'),
            'periodekeuangan_id' => $request->input('periodekeuangan_id'),
            'status' => '0',
            'pengguna_id' => Auth::user()->id,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pendapatan::insert($data);

        if ($service) 
        {
            return redirect('/pendapatan');
        }
        else 
        {
            return redirect('/pendapatan/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'jumlah_pendapatan' => 'required|min:0|max:32',
            'keterangan' => 'required|min:0|max:255',
            'periodekeuangan_id' => 'required',
            'tanggal' => 'required'
        ]);

        $id = $request->input('id');

        $data = [
            'jumlah_pendapatan' => $request->input('jumlah_pendapatan'),
            'keterangan' => $request->input('keterangan'),
            'tanggal' => $request->input('tanggal'),
            'periodekeuangan_id' => $request->input('periodekeuangan_id'),
            'status' => '0',
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pendapatan::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pendapatan');
        }
        else 
        {
            return redirect('/pendapatan/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pendapatan::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pendapatan');
        }
        else 
        {
            return redirect('/pendapatan');
        }
    }

    public function approve(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $data = Pendapatan::where(['id' => $id])->first();
        $payload = [
            'status' => $data->status == '1' ? '0' : '1',
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pendapatan::where(['id' => $id])->update($payload);

        if ($service) 
        {
            return redirect('/pendapatan');
        }
        else 
        {
            return redirect('/pendapatan');
        }
    }
}
