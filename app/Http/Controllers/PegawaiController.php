<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;

class PegawaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Pegawai::orderBy('id', 'desc')->paginate(10);
        return view('pegawai.index', ['data' => $data]);
    }

    public function search()
    {
        $search = $_GET['search'];
        $data = Pegawai::select(
            '*'
        )
        ->where('pegawai.nip', 'like', '%' . $search . '%')
        ->orWhere('pegawai.nama', 'like', '%' . $search . '%')
        ->orderBy('id', 'desc')
        ->paginate(10);
        return view('pegawai.index', ['data' => $data, 'search' => $search]);
    }

    public function create()
    {
        return view('pegawai.form');
    }

    public function edit($id)
    {
        $data = Pegawai::where(['id' => $id])->first();
        return view('pegawai.form', ['data' => $data]);
    }

    // crud
    public function save(Request $request)
    {
        $this->validate($request, [
            'nip' => 'required|min:0|max:32|unique:pegawai',
            'nama' => 'required|min:0|max:150',
            'jabatan' => 'required|min:0|max:150'
        ]);

        $data = [
            'nip' => $request->input('nip'),
            'nama' => $request->input('nama'),
            'jabatan' => $request->input('jabatan'),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pegawai::insert($data);

        if ($service) 
        {
            return redirect('/pegawai');
        }
        else 
        {
            return redirect('/pegawai/create');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nama' => 'required|min:0|max:150',
            'jabatan' => 'required|min:0|max:150'
        ]);

        $id = $request->input('id');

        $data = [
            'nama' => $request->input('nama'),
            'jabatan' => $request->input('jabatan'),
            "updated_at" => date('Y-m-d H:i:s')
        ];

        $service = Pegawai::where(['id' => $id])->update($data);

        if ($service) 
        {
            return redirect('/pegawai');
        }
        else 
        {
            return redirect('/pegawai/edit/'.$id);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $id = $request->input('id');

        $service = Pegawai::where(['id' => $id])->delete();

        if ($service) 
        {
            return redirect('/pegawai');
        }
        else 
        {
            return redirect('/pegawai');
        }
    }
}
