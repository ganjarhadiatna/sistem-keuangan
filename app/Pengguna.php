<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pengguna extends Authenticatable
{
    use Notifiable;

    protected $table = 'pengguna';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'nama', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'pengguna.id',
            'pengguna.username',
            'pengguna.email',
            'pengguna.nama',
            'pegawai.jabatan',
            'pengguna.created_at',
            'pengguna.updated_at'
        )
        ->join('pegawai', 'pegawai.id', '=', 'pengguna.pegawai_id')
        ->paginate($limit);
    }

    public function scopeGetPegawaById($queey, $id)
    {
        return $this
        ->select(
            'pegawai.id',
            'pegawai.nip',
            'pegawai.jabatan',
            'pengguna.username',
            'pengguna.nama'
        )
        ->join('pegawai', 'pegawai.id', '=', 'pengguna.pegawai_id')
        ->where('pengguna.id', $id)
        ->first();
    }
}
