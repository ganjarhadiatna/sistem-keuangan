<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendapatan extends Model
{
    protected $table = 'pendapatan';

    public function scopeGetAll($queey, $limit)
    {
        return $this
        ->select(
            'pendapatan.id',
            'pendapatan.tanggal',
            'pendapatan.keterangan',
            'pendapatan.jumlah_pendapatan',
            'pendapatan.status',
            'pendapatan.created_at',
            'pendapatan.updated_at',
            'pengguna.nama',
            'periodekeuangan.tahun'
        )
        ->join('pengguna', 'pengguna.id', '=', 'pendapatan.pengguna_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'pendapatan.periodekeuangan_id')
        ->orderBy('pendapatan.id', 'desc')
        ->paginate($limit);
    }

    public function scopeGetPegawaById($queey, $id)
    {
        return $this
        ->select(
            'pendapatan.id',
            'pendapatan.tanggal',
            'pendapatan.keterangan',
            'pendapatan.jumlah_pendapatan',
            'pendapatan.status',
            'pendapatan.created_at',
            'pendapatan.updated_at',
            'pengguna.nama',
            'periodekeuangan.tahun'
        )
        ->join('pengguna', 'pengguna.id', '=', 'pendapatan.pengguna_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'pendapatan.periodekeuangan_id')
        ->where('pendapatan.id', $id)
        ->first();
    }

    public function scopeGetByPeriode($queey, $periode)
    {
        return $this
        ->select(
            'pendapatan.id',
            'pendapatan.tanggal',
            'pendapatan.keterangan',
            'pendapatan.jumlah_pendapatan',
            'pengguna.nama',
            'periodekeuangan.tahun'
        )
        ->join('pengguna', 'pengguna.id', '=', 'pendapatan.pengguna_id')
        ->join('periodekeuangan', 'periodekeuangan.id', '=', 'pendapatan.periodekeuangan_id')
        ->where('periodekeuangan.id', $periode)
        ->get();
    }
}
