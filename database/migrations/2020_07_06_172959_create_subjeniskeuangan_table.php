<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjeniskeuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjeniskeuangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('subjenis_keuangan');
            $table->unsignedBigInteger('jeniskeuangan_id');
            $table->timestamps();

            $table->foreign('jeniskeuangan_id')->references('id')->on('jeniskeuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjeniskeuangan');
    }
}
