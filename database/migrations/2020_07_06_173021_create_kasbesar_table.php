<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKasbesarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kasbesar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->string('keterangan');
            $table->string('debit', 32);
            $table->string('kredit', 32);
            $table->string('saldo', 32);
            $table->unsignedBigInteger('detailkeuangan_id');
            $table->timestamps();

            $table->foreign('detailkeuangan_id')->references('id')->on('detailkeuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kasbesar');
    }
}
