<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailkeuanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailkeuangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jumlah_keuangan', 32);
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('periodekeuangan_id');
            $table->unsignedBigInteger('subjeniskeuangan_id');
            $table->timestamps();

            $table->foreign('periodekeuangan_id')->references('id')->on('periodekeuangan');
            $table->foreign('subjeniskeuangan_id')->references('id')->on('subjeniskeuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailkeuangan');
    }
}
