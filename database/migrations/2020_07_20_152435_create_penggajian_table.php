<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggajianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penggajian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gaji');
            $table->string('tunjangankesehatan');
            $table->string('tunjanganhariraya');
            $table->string('potongan');
            $table->date('tanggal');
            $table->unsignedBigInteger('pegawai_id');
            $table->timestamps();

            $table->foreign('pegawai_id')->references('id')->on('pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggajian');
    }
}
