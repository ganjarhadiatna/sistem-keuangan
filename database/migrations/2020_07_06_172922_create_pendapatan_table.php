<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendapatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendapatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->string('keterangan');
            $table->string('jumlah_pendapatan', 32);
            $table->enum('status', ['0', '1'])->default('0');
            $table->unsignedBigInteger('pengguna_id');
            $table->unsignedBigInteger('periodekeuangan_id');
            $table->timestamps();

            $table->foreign('pengguna_id')->references('id')->on('pengguna');
            $table->foreign('periodekeuangan_id')->references('id')->on('periodekeuangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendapatan');
    }
}
