<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'nip' => '0231500008',
                'nama' => 'A. JAJAT SUDRAJAT',
                'jabatan' => 'OPERATOR EXCAVATOR',
                'tanggal' => '2015-01-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nip' => '0231700029',
                'nama' => 'A. JAJADI ANDRIANI',
                'jabatan' => 'HSE',
                'tanggal' => '2017-02-12',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nip' => '0231200002',
                'nama' => 'ASEP GUNAWAN NUGRAHA',
                'jabatan' => 'DIREKTUR',
                'tanggal' => '2012-04-01',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'nip' => '0231600034',
                'nama' => 'ASRI NUR AISYAH',
                'jabatan' => 'STAFF OPERASIONAL',
                'tanggal' => '2016-10-08',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'nip' => '0231600012',
                'nama' => 'BIMA SUCI',
                'jabatan' => 'OPERATOR WHEEL LOADER',
                'tanggal' => '2016-06-08',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'nip' => '0231600020',
                'nama' => 'EDI WAHYONO',
                'jabatan' => 'MANAGER PRODUKSI',
                'tanggal' => '2016-02-18',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'nip' => '0231700033',
                'nama' => 'GIAR HELMY MAULANA',
                'jabatan' => 'SALES/MARKETING',
                'tanggal' => '2017-03-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '8',
                'nip' => '0231600037',
                'nama' => 'HANDRIAN ARYA WIJAYA',
                'jabatan' => 'MANAGER OPERASIONAL',
                'tanggal' => '2016-12-21',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '9',
                'nip' => '0231500006',
                'nama' => 'MAHMUDI',
                'jabatan' => 'SEKURITI',
                'tanggal' => '2015-06-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '10',
                'nip' => '0231500009',
                'nama' => 'MUHAMAD NASIR',
                'jabatan' => 'OPERATOR WHEEL LOADER',
                'tanggal' => '2015-06-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '11',
                'nip' => '0231700026',
                'nama' => 'RASTO JAYA MONI WIRANTO',
                'jabatan' => 'OPERATOR EXCAVATOR',
                'tanggal' => '2017-12-09',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '12',
                'nip' => '0231500020',
                'nama' => 'RISCHA APRIANI',
                'jabatan' => 'MANAGER FINANCE',
                'tanggal' => '2015-02-18',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '13',
                'nip' => '0231600010',
                'nama' => 'SINTA NOVIANTI',
                'jabatan' => 'STAFF FINANCE',
                'tanggal' => '2016-07-16',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '14',
                'nip' => '0231700011',
                'nama' => 'TAJUDIN',
                'jabatan' => 'MEKANIK',
                'tanggal' => '2017-04-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '15',
                'nip' => 'SPL1111',
                'nama' => 'ADMIN',
                'jabatan' => 'ADMIN',
                'tanggal' => '2017-12-02',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('pegawai')->insert($data);
    }
}
