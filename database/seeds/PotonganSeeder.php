<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PotonganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya makan',
                'jumlah' => '500000',
                'pegawai_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya makan',
                'jumlah' => '500000',
                'pegawai_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'tanggal' => date('Y-m-d H:i:s'),
                'keterangan' => 'biaya makan',
                'jumlah' => '500000',
                'pegawai_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('potongan')->insert($data);
    }
}
