<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjenisKeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'subjenis_keuangan' => 'Perlengkapan Pegawai',
                'jeniskeuangan_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'subjenis_keuangan' => 'HPP Sparepart Proyek',
                'jeniskeuangan_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'subjenis_keuangan' => 'Penggajian',
                'jeniskeuangan_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'subjenis_keuangan' => 'Perjalanan Dinas',
                'jeniskeuangan_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'subjenis_keuangan' => 'Fotocopy',
                'jeniskeuangan_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'subjenis_keuangan' => 'BBM, Parkir, Tol',
                'jeniskeuangan_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'subjenis_keuangan' => 'Listrik, Telpon, Internet',
                'jeniskeuangan_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('subjeniskeuangan')->insert($data);
    }
}
