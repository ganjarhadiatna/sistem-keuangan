<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenggunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'nama' => 'ASEP GUNAWAN NUGRAHA',
                'username' => 'asep',
                'email' => 'asep@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'pegawai_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'nama' => 'RISCHA APRIANI',
                'username' => 'rischa',
                'email' => 'rischa@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'pegawai_id' => '12',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'nama' => 'SINTA NOVIANTI',
                'username' => 'sinta',
                'email' => 'sinta@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'pegawai_id' => '13',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'nama' => 'ADMIN',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$HM72jVVcLhqE3NYilBvvVuylKf4/jOC4CReztgKuFnbIu2/Q6Dlb2',
                'pegawai_id' => '15',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pengguna')->insert($data);
    }
}
