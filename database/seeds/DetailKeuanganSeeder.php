<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetailKeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'jumlah_keuangan' => '7172000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '1',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'jumlah_keuangan' => '9169843200',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '2',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'jumlah_keuangan' => '1340400000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '3',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'jumlah_keuangan' => '100535000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '4',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'jumlah_keuangan' => '393000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '5',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'jumlah_keuangan' => '83797000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '6',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'jumlah_keuangan' => '7290000',
                'periodekeuangan_id' => '1',
                'subjeniskeuangan_id' => '7',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('detailkeuangan')->insert($data);
    }
}
