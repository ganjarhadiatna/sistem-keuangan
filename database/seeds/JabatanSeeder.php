<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'jabatan' => 'OPERATOR EXCAVATOR',
                'keterangan' => 'OPERATOR EXCAVATOR',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'jabatan' => 'HSE',
                'keterangan' => 'HSE',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'jabatan' => 'DIREKTUR',
                'keterangan' => 'DIREKTUR',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'jabatan' => 'STAFF OPERASIONAL',
                'keterangan' => 'STAFF OPERASIONAL',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'jabatan' => 'OPERATOR WHEEL LOADER',
                'keterangan' => 'OPERATOR WHEEL LOADER',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '6',
                'jabatan' => 'MANAGER PRODUKSI',
                'keterangan' => 'MANAGER PRODUKSI',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '7',
                'jabatan' => 'SALES/MARKETING',
                'keterangan' => 'SALES/MARKETING',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '8',
                'jabatan' => 'MANAGER OPERASIONAL',
                'keterangan' => 'MANAGER OPERASIONAL',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '9',
                'jabatan' => 'SEKURITI',
                'keterangan' => 'SEKURITI',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '10',
                'jabatan' => 'OPERATOR WHEEL LOADER',
                'keterangan' => 'OPERATOR WHEEL LOADER',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '11',
                'jabatan' => 'OPERATOR EXCAVATOR',
                'keterangan' => 'OPERATOR EXCAVATOR',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '12',
                'jabatan' => 'MANAGER FINANCE',
                'keterangan' => 'MANAGER FINANCE',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '13',
                'jabatan' => 'STAFF FINANCE',
                'keterangan' => 'STAFF FINANCE',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '14',
                'jabatan' => 'MEKANIK',
                'keterangan' => 'MEKANIK',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('jabatan')->insert($data);
    }
}
