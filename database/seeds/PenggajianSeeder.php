<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenggajianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $payload = [
            [
                'id' => 1,
                'gaji' => '3000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '1'
            ],
            [
                'id' => 2,
                'gaji' => '14000000',
                'tunjangankesehatan' => '1000000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '2'
            ],
            [
                'id' => 3,
                'gaji' => '18000000',
                'tunjangankesehatan' => '1200000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '3'
            ],
            [
                'id' => 4,
                'gaji' => '6000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '4'
            ],
            [
                'id' => 5,
                'gaji' => '3000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '5'
            ],
            [
                'id' => 6,
                'gaji' => '11000000',
                'tunjangankesehatan' => '1000000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '6'
            ],
            [
                'id' => 7,
                'gaji' => '3500000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '7'
            ],
            [
                'id' => 8,
                'gaji' => '11000000',
                'tunjangankesehatan' => '1000000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '8'
            ],
            [
                'id' => 9,
                'gaji' => '2500000',
                'tunjangankesehatan' => '500000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '9'
            ],
            [
                'id' => 10,
                'gaji' => '3000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '10'
            ],
            [
                'id' => 11,
                'gaji' => '3000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '11'
            ],
            [
                'id' => 12,
                'gaji' => '13000000',
                'tunjangankesehatan' => '1000000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '12'
            ],
            [
                'id' => 13,
                'gaji' => '6000000',
                'tunjangankesehatan' => '600000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '13'
            ],
            [
                'id' => 14,
                'gaji' => '4000000',
                'tunjangankesehatan' => '500000',
                'tunjanganhariraya' => '0',
                'potongan' => '0',
                'pegawai_id' => '14'
            ]
        ];

        for ($i=0; $i < 12; $i++) {
            $month = ($i < 10) ? '0'.($i + 1) : ($i + 1);
            $lates = count($payload) * ($i + 1);
            $id = 1;
            foreach ($payload as $dt) {
                $newPayload = [
                    'id' => ($lates + $id),
                    'gaji' => $dt['gaji'],
                    'tunjangankesehatan' => $dt['tunjangankesehatan'],
                    'tunjanganhariraya' => (($i + 1) == 6) ? $dt['gaji'] : $dt['tunjanganhariraya'],
                    'potongan' => $dt['potongan'],
                    'tanggal' => '2018-'.$month.'-25',
                    'pegawai_id' => $dt['pegawai_id'],
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s')
                ];
                $id++;
                array_push($data, $newPayload);
            }
        }
        
        DB::table('penggajian')->insert($data);
    }
}
