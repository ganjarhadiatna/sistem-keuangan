<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            'JabatanSeeder',
            'PegawaiSeeder',
            'PenggunaSeeder',
            'PeriodeKeuanganSeeder',
            'JenisKeuanganSeeder',
            'SubjenisKeuanganSeeder',
            'DetailKeuanganSeeder',
            'PendapatanSeeder',
            'KasbesarSeeder',
            'PenggajianSeeder'
        ]);
    }
}
