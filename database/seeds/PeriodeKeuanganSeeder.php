<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PeriodeKeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'tanggal' => date('Y-m-d'),
                'tahun' => '2018',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'tanggal' => date('Y-m-d'),
                'tahun' => '2019',
                'status' => '0',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'tanggal' => date('Y-m-d'),
                'tahun' => '2020',
                'status' => '0',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('periodekeuangan')->insert($data);
    }
}
