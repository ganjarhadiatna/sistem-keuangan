<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KasbesarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'debit' => '0',
                'kredit' => '0',
                'saldo' => '0',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Kas besar saat ini',
                'detailkeuangan_id' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'debit' => '0',
                'kredit' => '0',
                'saldo' => '0',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Kas besar saat ini',
                'detailkeuangan_id' => '2',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'debit' => '0',
                'kredit' => '0',
                'saldo' => '0',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Kas besar saat ini',
                'detailkeuangan_id' => '3',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'debit' => '0',
                'kredit' => '0',
                'saldo' => '0',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Kas besar saat ini',
                'detailkeuangan_id' => '4',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '5',
                'debit' => '0',
                'kredit' => '0',
                'saldo' => '0',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Kas besar saat ini',
                'detailkeuangan_id' => '5',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('kasbesar')->insert($data);
    }
}
