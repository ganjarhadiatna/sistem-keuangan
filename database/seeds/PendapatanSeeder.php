<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PendapatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'jumlah_pendapatan' => '15000000',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Penjualan Material Tanah',
                'pengguna_id' => '1',
                'periodekeuangan_id' => '1',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'jumlah_pendapatan' => '18000000',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Sewa Kendaraan',
                'pengguna_id' => '1',
                'periodekeuangan_id' => '1',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'jumlah_pendapatan' => '25000000',
                'tanggal' => date('Y-m-d'),
                'keterangan' => 'Sewa Alat Berat',
                'pengguna_id' => '1',
                'periodekeuangan_id' => '1',
                'status' => '1',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('pendapatan')->insert($data);
    }
}
