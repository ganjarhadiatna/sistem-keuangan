<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisKeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'jenis_keuangan' => 'Biaya Peralatan',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'jenis_keuangan' => 'Biaya HPP',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'jenis_keuangan' => 'Biaya Penggajian',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ],
            [
                'id' => '4',
                'jenis_keuangan' => 'Biaya Beban Perusahaan',
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            ]
        ];

        DB::table('jeniskeuangan')->insert($data);
    }
}
