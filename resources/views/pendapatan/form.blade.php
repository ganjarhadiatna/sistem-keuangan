@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Pendapatan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Pendapatan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-pendapatan-update') }} @else {{ route('ui-pendapatan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPeriode">Periode Keuangan</label>
                                <select value="@if(isset($data)) {{ $data->periodekeuangan_id }} @endif" class="form-control @error('periodekeuangan_id') is-invalid @enderror" id="inputPeriode" name="periodekeuangan_id">
                                @foreach($periode as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->periodekeuangan_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->tahun }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->tahun }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->tahun }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('periodekeuangan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputPendapatan">Jumlah Pendapatan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->jumlah_pendapatan }} @endif" class="form-control @error('jumlah_pendapatan') is-invalid @enderror" id="inputPendapatan" name="jumlah_pendapatan" required>
                            @error('jumlah_pendapatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKeterangan">Keterangan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->keterangan }} @endif" class="form-control @error('keterangan') is-invalid @enderror" id="inputKeterangan" name="keterangan" required>
                            @error('keterangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTanggal">Tanggal</label>
                            <input type="date" value="@if(isset($data)) {{ date_format(date_create($data->tanggal), 'm/d/Y') }} @endif" class="form-control @error('tanggal') is-invalid @enderror" id="inputTanggal" name="tanggal" required>
                            @error('tanggal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
