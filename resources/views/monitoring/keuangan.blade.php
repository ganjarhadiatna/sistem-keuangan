@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Monitoring Keuangan</h3>
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Pos Keuangan</th>
                                <th scope="col">Rencana Keuangan</th>
                                <th scope="col">Saldo</th>
                                <th scope="col">Realisasi</th>
                                <th scope="col">Persentase</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            @foreach($data as $dt)
                                <?php $rencana_keuangan = ($dt->jumlah_keuangan + $dt->realisasi) ?>
                                <?php $jumlah_keuangan = $dt->jumlah_keuangan ?>
                                <?php $realisasi = $dt->realisasi ?>
                                <?php $persentase = ($realisasi / $rencana_keuangan) * 100 ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $dt->tahun.' - '.$dt->jenis_keuangan.' - '.$dt->subjenis_keuangan }}</td>
                                    <td>{{ "Rp " . number_format($rencana_keuangan,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($jumlah_keuangan,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($realisasi,2,',','.') }}</td>
                                    <td>{{ number_format($persentase, 2) }} %</td>
                                    <td>
                                        @if($dt->realisasi < $dt->rencana_keuangan)
                                            <div>Tidak Aman</div>
                                        @else
                                            <div>Aman</div>
                                        @endif
                                     </td>
                                </tr>
                                <?php $i++ ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
