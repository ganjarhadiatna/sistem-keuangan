@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper justify-content-center">
                    <h3 style="margin-top: 6px;">Laporan Rugi Laba {{ $periode }}</h3>
                </div>

                <div class="card-body">
                    <div class="col-md-10" style="margin: auto;">
                        <div style="text-align: center; font-size: 20pt;">
                            <h3>
                                PT. SENTOSA MITRA SEMESTA
                            </h3>
                            <h3>
                                LAPORAN RUGI LABA
                            </h3>
                            <h3>
                                PERIODE 31 DESEMBER {{ $periode }}
                            </h3>
                        </div>

                        <div style="padding: 10px;"></div>

                        <!-- pendapatan -->
                        <div class="container-fluid">
                            <b>Pendapatan</b>
                            <?php $total = 0 ?>
                            @foreach($pendapatan as $dt)
                                <div class="container-fluid wrapper" style="margin-bottom: 5px;">
                                    <div class="mr-auto">{{ $dt->keterangan }}</div>
                                    <div class="ml-auto">{{ "Rp " . number_format(isset($dt->jumlah_pendapatan) ? $dt->jumlah_pendapatan : 0 ,2,',','.') }}</div>
                                </div>
                                <?php $total += $dt->jumlah_pendapatan ?>
                            @endforeach
                        </div>

                        <div class="container-fluid">
                            <div class="container-fluid wrapper" style="margin-top: 15px;">
                                <div class="mr-auto"><b>Total Pendapatan</b></div>
                                <div class="ml-auto"><b>{{ "Rp " . number_format(isset($total) ? $total : 0 ,2,',','.') }}</b></div>
                            </div>
                        </div>

                        <div style="padding: 10px;"></div>

                        <!-- beban biaya perusahaan -->
                        <div class="container-fluid">
                            <b>Beban Biaya Perusahaan</b>
                            <?php $total2 = 0 ?>
                            @foreach($keuangan as $dt)
                                <div class="container-fluid wrapper" style="margin-bottom: 5px;">
                                    <div class="mr-auto">{{ $dt->keterangan }}</div>
                                    <div class="ml-auto">{{ "Rp " . number_format(isset($dt->kredit) ? $dt->kredit : 0 ,2,',','.') }}</div>
                                </div>
                                <?php $total2 += $dt->kredit ?>
                            @endforeach
                        </div>

                        <div class="container-fluid">
                            <div class="container-fluid wrapper" style="margin-top: 15px;">
                                <div class="mr-auto"><b>Total Pengeluaran</b></div>
                                <div class="ml-auto"><b>{{ "Rp " . number_format(isset($total2) ? $total2 : 0 ,2,',','.') }}</b></div>
                            </div>
                        </div>

                        <div style="padding: 20px;"></div>

                        <?php $labaUasaha = $total - $total2 ?>

                        <div class="container-fluid">
                            <div class="wrapper" style="margin-top: 5px;">
                                <div class="mr-auto"><b>Laba Usaha</b></div>
                                <div class="ml-auto"><b>{{ "Rp " . number_format($labaUasaha ,2,',','.') }}</b></div>
                            </div>
                        </div>

                        <?php $pajak = 10032069 ?>

                        <div class="container-fluid">
                            <div class="wrapper" style="margin-top: 5px;">
                                <div class="mr-auto"><b>Pajak</b></div>
                                <div class="ml-auto"><b>{{ "Rp " . number_format($pajak ,2,',','.') }}</b></div>
                            </div>
                        </div>

                        <?php $labaBersih = $labaUasaha - $pajak ?>

                        <div class="container-fluid">
                            <div class="wrapper" style="margin-top: 5px;">
                                <div class="mr-auto"><b>Laba Bersih</b></div>
                                <div class="ml-auto"><b>{{ "Rp " . number_format($labaBersih ,2,',','.') }}</b></div>
                            </div>
                        </div>

                        <div style="padding: 20px;"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
