@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <div>BUKU BESAR PT. SENTOSA MITRA SEMESTA</div>
                </div>
                <div class="card-body">
                    <form method="GET" action="{{ route('ui-report-detailbukubesar') }}">
                        <div class="wrapper">
                            <div class="container-fluid">
                                <label for="inputPeriodeKeuangan">Periode Keuangan</label>
                                <div class="form-group" style="width: 100%; margin-right: 15px;">
                                    <div class="form-group">
                                        <select value="@if(isset($periode)) {{ $periode }} @endif" class="form-control @error('periode_keuangan') is-invalid @enderror" id="inputPeriodeKeuangan" name="periode_keuangan">
                                        @foreach($periode as $dt)
                                            @if(isset($tahun))
                                                @if($tahun == $dt->tahun)
                                                    <option value="{{ $dt->tahun }}" selected>{{ $dt->tahun }}</option>
                                                @else
                                                    <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    @error('periode_keuangan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="container-fluid">
                                <label for="inputBulan">Bulan</label>
                                <div class="form-group" style="width: 100%; margin-right: 15px;">
                                    <div class="form-group">
                                        <select value="@if(isset($bulan_keuangan)) {{ $bulan_keuangan }} @endif" class="form-control @error('bulan') is-invalid @enderror" id="inputBulan" name="bulan">
                                        @foreach($bulan as $dt)
                                            @if(isset($bulan_keuangan))
                                                @if($bulan_keuangan == $dt['id'])
                                                    <option value="{{ $dt['id'] }}" selected>{{ $dt['month'] }}</option>
                                                @else
                                                    <option value="{{ $dt['id'] }}">{{ $dt['month'] }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $dt['id'] }}">{{ $dt['month'] }}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    @error('bulan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="container-fluid">
                                <label for="inputPosKeuangan">Pos Keuangan</label>
                                <div class="form-group" style="width: 100%; margin-right: 15px;">
                                    <div class="form-group">
                                        <select value="@if(isset($pos_keuangan)) {{ $pos_keuangan }} @endif" class="form-control @error('pos_keuangan') is-invalid @enderror" id="inputPosKeuangan" name="pos_keuangan">
                                        @foreach($pos as $dt)
                                            @if(isset($pos_keuangan))
                                                @if($pos_keuangan == $dt->id)
                                                    <option value="{{ $dt->id }}" selected>{{ $dt->jenis_keuangan }}</option>
                                                @else
                                                    <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan }}</option>
                                                @endif
                                            @else
                                                <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan }}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    @error('pos_keuangan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="container-fluid wrapper">
                            <div class="mr-auto"></div>
                            <div class="ml-auto">
                                <button type="submit" class="btn btn-primary">Proses</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if(isset($data))
                <div class="card" style="margin-top: 20px;">
                    <div class="card-header">
                        <div>LAPORAN BUKU BESAR</div>
                    </div>
                    <div class="card-body">
                        <div class="container-fluid" style="margin: auto;">
                            <div style="text-align: center; font-size: 20pt;">
                                <h3>
                                    PT. SENTOSA MITRA SEMESTA
                                </h3>
                                @foreach($bulan as $dt)
                                    @if($dt['id'] == $bulan_keuangan)
                                        <h3>
                                            LAPORAN BUKU BESAR BULAN {{ strtoupper($dt['month']) }} {{ $tahun }}
                                        </h3>
                                    @endif
                                @endforeach
                                @foreach($pos as $dt)
                                    @if($dt->id == $pos_keuangan)                                
                                        <h3>
                                            POS KEUANGAN {{ strtoupper($dt->jenis_keuangan) }}
                                        </h3>
                                    @endif
                                @endforeach
                            </div>

                            <div style="padding: 10px;"></div>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Keuangan</th>
                                        <th scope="col">Deskripsi</th>
                                        <th scope="col">Saldo</th>
                                        <th scope="col">Debit</th>
                                        <th scope="col">Kredit</th>
                                        <th scope="col">Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($data as $dt)
                                        <tr>
                                            <th scope="row">{{ $i }}</th>
                                            <td>{{ $dt->subjenis_keuangan }}</td>
                                            <td>{{ $dt->keterangan }}</td>
                                            <td>{{ "Rp " . number_format($dt->saldo,2,',','.') }}</td>
                                            <td>{{ "Rp " . number_format($dt->debit,2,',','.') }}</td>
                                            <td>{{ "Rp " . number_format($dt->kredit,2,',','.') }}</td>
                                            <td>{{ $dt->tanggal }}</td>
                                        </tr>
                                        <?php $i++; ?>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection