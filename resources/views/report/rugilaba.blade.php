@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Laporan Rugi Laba</h3>
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Periode</th>
                                <th scope="col">Pendapatan</th>
                                <th scope="col">Pengeluaran</th>
                                <th scope="col">Rugi Laba</th>
                                <th width="100"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            @foreach($data as $dt)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $dt->tahun }}</td>
                                    <td>{{ "Rp " . number_format(isset($dt->pendapatan) ? $dt->pendapatan : 0 ,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format(isset($dt->pengeluaran) ? $dt->pengeluaran : 0 ,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format(isset($dt->pendapatan) && isset($dt->pengeluaran) ? ($dt->pendapatan - $dt->pengeluaran) : 0 ,2,',','.') }}</td>
                                    <td>
                                        <a href="{{ route('ui-report-rugilaba-detail', $dt->tahun)}}" class="btn btn-primary">
                                            Detail
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++ ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
