@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <div>JURNAL UMUM PT. SENTOSA MITRA SEMESTA</div>
                </div>
                <div class="card-body">
                    <form method="GET" action="{{ route('ui-report-detailjurnalumum') }}">
                        <label for="inputPeriodeKeuangan">Periode Keuangan</label>
                        <div class="wrapper">
                            <div class="form-group" style="width: 100%; margin-right: 15px;">
                                <div class="form-group">
                                    <select value="@if(isset($periode)) {{ $periode }} @endif" class="form-control @error('periode_keuangan') is-invalid @enderror" id="inputPeriodeKeuangan" name="periode_keuangan">
                                    @foreach($periode as $dt)
                                        @if(isset($tahun))
                                            @if($tahun == $dt->tahun)
                                                <option value="{{ $dt->tahun }}" selected>{{ $dt->tahun }}</option>
                                            @else
                                                <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                                @error('periode_keuangan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">Proses</button>
                            </div>
                        </div>
                    </form>

                    @if(isset($data))
                        <div class="col-md-10" style="margin: auto;">
                            <div style="text-align: center; font-size: 20pt;">
                                <h3>
                                    PT. SENTOSA MITRA SEMESTA
                                </h3>
                                <h3>
                                    LAPORAN JURNAL UMUM
                                </h3>
                                <h3>
                                    PERIODE 31 DESEMBER {{ $tahun }}
                                </h3>
                            </div>

                            <div style="padding: 10px;"></div>

                            <div class="container-fluid">
                                <div class="container-fluid wrapper" style="margin-bottom: 5px;">
                                    <div style="width: 500px;"><b>Keterangan</b></div>
                                    <div style="width: 250px"><b>Debit</b></div>
                                    <div style="width: 250px"><b>Kredit</b></div>
                                </div>

                                <?php $totalKredit = 0 ?>
                                <?php $totalDebit = 0 ?>
                                @foreach($data as $dt)
                                    <div class="container-fluid wrapper" style="margin-bottom: 5px;">
                                        <div style="width: 500px;">{{ $dt->keterangan }}</div>
                                        <div style="width: 250px">{{ "Rp " . number_format(isset($dt->debit) ? $dt->debit : 0 ,2,',','.') }}</div>
                                        <div style="width: 250px">{{ "Rp " . number_format(isset($dt->kredit) ? $dt->kredit : 0 ,2,',','.') }}</div>
                                    </div>
                                    <?php $totalKredit += $dt->kredit ?>
                                    <?php $totalDebit += $dt->debit ?>
                                @endforeach

                                <div class="container-fluid wrapper" style="margin-bottom: 5px;">
                                    <div style="width: 500px;"><b>Total</b></div>
                                    <div style="width: 250px"><b>{{ "Rp " . number_format($totalDebit,2,',','.') }}</b></div>
                                    <div style="width: 250px"><b>{{ "Rp " . number_format($totalKredit,2,',','.') }}</b></div>
                                </div>
                            </div>

                        </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
</div>
@endsection