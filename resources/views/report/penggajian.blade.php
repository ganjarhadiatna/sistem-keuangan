@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h3 style="margin-top: 6px;">Laporan Penggajian</h3>
                </div>
                <div class="card-body">
                    <form method="GET" action="{{ route('ui-report-penggajian') }}">
                        <div class="wrapper">
                            <div style="width: 100%;">
                                <label for="inputPeriodeKeuangan">Pilih Periode Keuangan</label>
                                <div class="form-group" style="width: 100%; margin-right: 15px;">
                                    <div class="form-group">
                                        <select value="@if(isset($selected_periode)) {{ $selected_periode }} @endif" class="form-control "id="inputPeriodeKeuangan" name="periode">
                                        <option>Periode Keuangan</option>
                                        @foreach($periode as $dt)
                                            @if($selected_periode == $dt->tahun)
                                                <option value="{{ $dt->tahun }}" selected>{{ $dt->tahun }}</option>
                                            @else
                                                <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; margin-left: 15px; margin-right: 15px;">
                                <label for="inputBulan">Pilih Bulan</label>
                                <div class="form-group" style="width: 100%; margin-right: 15px;">
                                    <div class="form-group">
                                        <select value="@if(isset($selected_month)) {{ $selected_month }} @endif" class="form-control" id="inputBulan" name="bulan">
                                        <option>Bulan</option>
                                        @foreach($month as $dt)
                                            @if($selected_month == $dt['id'])
                                                <option value="{{ $dt['id'] }}" selected>{{ $dt['month'] }}</option>
                                            @else
                                                <option value="{{ $dt['id'] }}">{{ $dt['month'] }}</option>
                                            @endif
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 150px;">
                                <div style="height: 30px;"></div>
                                <button type="submit" class="btn btn-primary" style="width: 100%;">Proses</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card" style="margin-top: 15px;">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Data Penggajian</h3>
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIP</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Gaji</th>
                                <th scope="col">Tunjangan Kesehatan</th>
                                <th scope="col">Tunjangan Hari Raya</th>
                                <th scope="col">Potongan</th>
                                <th scope="col">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->nip }}</td>
                                    <td>{{ $dt->nama }}</td>
                                    <td>{{ "Rp " . number_format($dt->gaji,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($dt->tunjangankesehatan,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($dt->tunjanganhariraya,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($dt->potongan,2,',','.') }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
