<nav id="sidebar">
    <div class="sidebar-header" style="text-align: center;">
        <img style="position: relative; width: 150px; margin: auto;" src="{{ asset('img/logo2.jpeg') }}" />
    </div>

    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'ADMIN')
        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Data Master</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="{{ route('ui-pegawai') }}">Pegawai</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-pengguna') }}">Pengguna</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'DIREKTUR')
        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li>
                <a href="{{ route('ui-monitoring-keuangan') }}">Monitoring Keuangan</a>
            </li>
            <li>
                <a href="#menuKeuangan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Kelola Rencana Keuangan</a>
                <ul class="collapse list-unstyled" id="menuKeuangan">
                    <li>
                        <a href="{{ route('ui-detail-keuangan') }}">Konfirmasi Rencana Keuangan</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#menuPendapatan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Kelola Pendapatan</a>
                <ul class="collapse list-unstyled" id="menuPendapatan">
                    <li>
                        <a href="{{ route('ui-pendapatan') }}">Konfirmasi Pendapatan</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#menuLaporan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Laporan Keuangan</a>
                <ul class="collapse list-unstyled" id="menuLaporan">
                    <li>
                        <a href="{{ route('ui-report-penggajian') }}">Laporan Penggajian</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-report-rugilaba') }}">Laporan Rugi Laba</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'MANAGER FINANCE')
        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li>
                <a href="#menuKeuangan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Kelola Rencana Keuangan</a>
                <ul class="collapse list-unstyled" id="menuKeuangan">
                    <li>
                        <a href="{{ route('ui-periode-keuangan') }}">Kelola Periode Keuangan</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-jenis-keuangan') }}">Kelola Jenis Keuangan</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-subjenis-keuangan') }}">Kelola Subjenis Keuangan</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-detail-keuangan') }}">Kelola Detail Keuangan</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('ui-penggajian') }}">Kelola Penggajian</a>
            </li>
            <li>
                <a href="{{ route('ui-pendapatan') }}">Kelola Pendapatan</a>
            </li>
            <li>
                <a href="#menuLaporan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Laporan Keuangan</a>
                <ul class="collapse list-unstyled" id="menuLaporan">
                    <li>
                        <a href="{{ route('ui-report-jurnalumum') }}">Laporan Jurnal Umum</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-report-bukubesar') }}">Laporan Buku Besar</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-report-rugilaba') }}">Laporan Rugi Laba</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE')
        <ul class="list-unstyled components">
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li>
                <a href="#menuKeuangan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Kelola Transaksi</a>
                <ul class="collapse list-unstyled" id="menuKeuangan">
                    <li>
                        <a href="{{ route('ui-kasbesar-create') }}">Penambahan Transaksi</a>
                    </li>
                    <li>
                        <a href="{{ route('ui-kasbesar') }}">Laporan Transaksi</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endif

</nav>