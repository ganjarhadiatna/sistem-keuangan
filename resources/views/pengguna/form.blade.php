@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Pengguna</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Pengguna</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-pengguna-update') }} @else {{ route('ui-pengguna-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPegawai">Pegawai</label>
                                @if(isset($data))
                                    <select value="@if(isset($data)) {{ $data->pegawai_id }} @endif" class="form-control @error('pegawai_id') is-invalid @enderror" id="inputPegawai" name="pegawai_id" readonly>
                                @else
                                    <select value="@if(isset($data)) {{ $data->pegawai_id }} @endif" class="form-control @error('pegawai_id') is-invalid @enderror" id="inputPegawai" name="pegawai_id">
                                @endif
                                @foreach($pegawai as $dt)
                                    @if(isset($data))
                                        @if($data->pegawai_id == $dt->id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->nama.' - '.$dt->jabatan }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->nama.' - '.$dt->jabatan }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('pegawai_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputNama">Nama</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" value="@if(isset($data)) {{ $data->email }} @endif" class="form-control @error('email') is-invalid @enderror" id="inputEmail" name="email" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="inputPassword" name="password" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
