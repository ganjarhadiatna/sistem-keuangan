@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Detail Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Detail Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-detail-keuangan-update') }} @else {{ route('ui-detail-keuangan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPeriode">Periode Keuangan</label>
                                <select value="@if(isset($data)) {{ $data->periodekeuangan_id }} @endif" class="form-control @error('periodekeuangan_id') is-invalid @enderror" id="inputPeriode" name="periodekeuangan_id">
                                @foreach($periode as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->periodekeuangan_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->tahun }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->tahun }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->tahun }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('periodekeuangan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputSubjenisKeuangan">Jenis Keuangan</label>
                                <select value="@if(isset($data)) {{ $data->subjeniskeuangan_id }} @endif" class="form-control @error('subjeniskeuangan_id') is-invalid @enderror" id="inputSubjenisKeuangan" name="subjeniskeuangan_id">
                                @foreach($subjenis as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->subjeniskeuangan_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->jenis_keuangan . ' - ' . $dt->subjenis_keuangan }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan . ' - ' . $dt->subjenis_keuangan }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan . ' - ' . $dt->subjenis_keuangan }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('subjeniskeuangan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputJumlahKeuangan">Jumlah Keuangan (Rp)</label>
                            <input type="text" value="@if(isset($data)) {{ $data->jumlah_keuangan }} @endif" class="form-control @error('jumlah_keuangan') is-invalid @enderror" id="inputJumlahKeuangan" name="jumlah_keuangan" required>
                            @error('jumlah_keuangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
