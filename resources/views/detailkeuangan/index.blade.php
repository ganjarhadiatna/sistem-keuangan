@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'MANAGER FINANCE')
                            <h3 style="margin-top: 6px;">Kelola Detail Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Konfirmasi Rencana Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto">
                        @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'MANAGER FINANCE')
                            <a href="{{ route('ui-detail-keuangan-create') }}" class="btn btn-primary">
                                <i class="fa fa-lw fa-plus"></i>
                            </a>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Rencana Keuangan</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">Periode</th>
                                <th scope="col">Status</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th scope="col">Tanggal Edit</th>
                                <th width="120"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->jenis_keuangan }} - {{ $dt->subjenis_keuangan }}</td>
                                    <td>{{ "Rp " . number_format($dt->jumlah_keuangan,2,',','.') }}</td>
                                    <td>{{ $dt->tahun }}</td>
                                    <td>{{ $dt->status == 1 ? 'Disetujui' : 'Ditolak' }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>{{ $dt->updated_at }}</td>
                                    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'MANAGER FINANCE')
                                        <td>
                                            <a href="{{ route('ui-detail-keuangan-edit', $dt->id) }}" class="btn btn-warning">
                                                <i class="fa fa-lw fa-pencil-alt"></i>
                                            </a>

                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                                <i class="fa fa-lw fa-trash-alt"></i>
                                            </button>

                                            <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    <div class="modal-body">
                                                        Data akan dihapus secara permanen, lanjutkan?
                                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <a class="btn btn-danger" 
                                                                href="{{ route('ui-detail-keuangan-delete') }}"
                                                                onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                                >
                                                                Lanjutkan
                                                            </a>

                                                            <form id="id-form-{{ $dt->id }}" action="{{ route('ui-detail-keuangan-delete') }}" method="POST" style="display: block;">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    @else
                                        <td>
                                            <button class="{{ $dt->status == 1 ? 'btn btn-danger' : 'btn btn-success' }}" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                                {{ $dt->status == 1 ? 'Tolak' : 'Setujui' }}
                                            </button>

                                            <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    <div class="modal-body">
                                                    {{ $dt->status == 1 ? 'Tolak rencana keuangan ini?' : 'Setujui rencana keuangan ini?' }}
                                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <a class="btn btn-danger" 
                                                                href="{{ route('ui-detail-keuangan-approve') }}"
                                                                onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                                >
                                                                Lanjutkan
                                                            </a>

                                                            <form id="id-form-{{ $dt->id }}" action="{{ route('ui-detail-keuangan-approve') }}" method="POST" style="display: block;">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
