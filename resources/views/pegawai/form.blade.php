@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Pegawai</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Pegawai</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-pegawai-update') }} @else {{ route('ui-pegawai-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <label for="inputNip">NIP</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->nip }} @endif" class="form-control @error('nip') is-invalid @enderror" id="inputNip" name="nip" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->nip }} @endif" class="form-control @error('nip') is-invalid @enderror" id="inputNip" name="nip" required>
                            @endif
                            @error('nip')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputNama">Nama</label>
                            <input type="text" value="@if(isset($data)) {{ $data->nama }} @endif" class="form-control @error('nama') is-invalid @enderror" id="inputNama" name="nama" required>
                            @error('nama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputJabatan">Jabatan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->jabatan }} @endif" class="form-control @error('jabatan') is-invalid @enderror" id="inputJabatan" name="jabatan" required>
                            @error('jabatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
