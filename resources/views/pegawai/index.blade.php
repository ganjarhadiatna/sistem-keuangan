@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        <h3 style="margin-top: 6px;">Pegawai</h3>
                    </div>
                    <div class="ml-auto">
                        <!-- <a href="{{ route('ui-pegawai-create') }}" class="btn btn-primary">
                            <i class="fa fa-lw fa-plus"></i>
                        </a> -->
                    </div>
                </div>

                <div class="card-body">
                    <form method="GET" action="{{ route('ui-pegawai-search') }}">
                        <div class="wrapper">
                            <div class="form-group" style="width: 100%; margin-right: 15px;">
                                <input type="text" value="@if(isset($search)) {{ $search }} @endif" class="form-control @error('search') is-invalid @enderror" id="inputSearch" name="search" placeholder="Cari berdasarkan nip atau nama.." required>
                                @error('periode_keuangan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">Cari</button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIP</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jabatan</th>
                                <th scope="col">Tanggal Masuk</th>
                                <th scope="col">Tanggal Dibuat</th>
                                <th scope="col">Tanggal Edit</th>
                                <!-- <th width="120"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->nip }}</td>
                                    <td>{{ $dt->nama }}</td>
                                    <td>{{ $dt->jabatan }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    <td>{{ $dt->created_at }}</td>
                                    <td>{{ $dt->updated_at }}</td>
                                    <!-- <td>
                                        <a href="{{ route('ui-pegawai-edit', $dt->id) }}" class="btn btn-warning">
                                            <i class="fa fa-lw fa-pencil-alt"></i>
                                        </a>

                                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                            <i class="fa fa-lw fa-trash-alt"></i>
                                        </button>

                                        <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                <div class="modal-body">
                                                    Data akan dihapus secara permanen, lanjutkan?
                                                </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        <a class="btn btn-danger" 
                                                            href="{{ route('ui-pegawai-delete') }}"
                                                            onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                            >
                                                            Lanjutkan
                                                        </a>

                                                        <form id="id-form-{{ $dt->id }}" action="{{ route('ui-pegawai-delete') }}" method="POST" style="display: block;">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td> -->
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
