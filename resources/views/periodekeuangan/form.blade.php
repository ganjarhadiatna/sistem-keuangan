@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Periode Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Periode Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-periode-keuangan-update') }} @else {{ route('ui-periode-keuangan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <label for="inputTanggal">Tanggal</label>
                            <input type="date" value="@if(isset($data)) {{ $data->tanggal }} @endif" class="form-control @error('tanggal') is-invalid @enderror" id="inputTanggal" name="tanggal" required>
                            @error('tanggal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTahun">Tahun</label>
                            <input type="text" value="@if(isset($data)) {{ $data->tahun }} @endif" class="form-control @error('tahun') is-invalid @enderror" id="inputTahun" name="tahun" required>
                            @error('tahun')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputStatus">Status</label>
                                <select value="@if(isset($data)) {{ $data->status }} @endif" class="form-control @error('status') is-invalid @enderror" id="inputStatus" name="status">
                                    @if(isset($data))
                                        @if($data->status == 1)
                                            <option value="1" selected>Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        @else
                                            <option value="1">Aktif</option>
                                            <option value="0" selected>Tidak Aktif</option>
                                        @endif
                                    @else
                                        <option value="1">Aktif</option>
                                        <option value="0">Tidak Aktif</option>
                                    @endif
                                </select>
                            </div>
                            @error('status')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
