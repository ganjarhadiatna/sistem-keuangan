@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Transaksi Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Transaksi Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-kasbesar-update') }} @else {{ route('ui-kasbesar-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputDetailkeuangan">Rencana Keuangan</label>
                                @if(isset($data))
                                    <select value="@if(isset($data)) {{ $data->detailkeuangan_id }} @endif" class="form-control @error('detailkeuangan_id') is-invalid @enderror" id="inputDetailkeuangan" name="detailkeuangan_id" readonly>
                                @else
                                    <select value="@if(isset($data)) {{ $data->detailkeuangan_id }} @endif" class="form-control @error('detailkeuangan_id') is-invalid @enderror" id="inputDetailkeuangan" name="detailkeuangan_id">
                                @endif
                                @foreach($detailkeuangan as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->detailkeuangan_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->jenis_keuangan.' - '.$dt->subjenis_keuangan.' - Rp '.number_format($dt->jumlah_keuangan,2,',','.') }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan.' - '.$dt->subjenis_keuangan.' - Rp '.number_format($dt->jumlah_keuangan,2,',','.') }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan.' - '.$dt->subjenis_keuangan.' - Rp '.number_format($dt->jumlah_keuangan,2,',','.') }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('detailkeuangan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKredit">Jumlah Transaksi</label>
                            @if(isset($data))
                                <input type="text" value="@if(isset($data)) {{ $data->kredit }} @endif" class="form-control @error('kredit') is-invalid @enderror" id="inputKredit" name="kredit" required readonly>
                            @else
                                <input type="text" value="@if(isset($data)) {{ $data->kredit }} @endif" class="form-control @error('kredit') is-invalid @enderror" id="inputKredit" name="kredit" required>
                            @endif
                            @error('kredit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputKeterangan">Keterangan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->keterangan }} @endif" class="form-control @error('keterangan') is-invalid @enderror" id="inputKeterangan" name="keterangan" required>
                            @error('keterangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTanggal">Tanggal</label>
                            <input type="date" value="@if(isset($data)) {{ $data->tanggal }} @endif" class="form-control @error('tanggal') is-invalid @enderror" id="inputTanggal" name="tanggal" required>
                            @error('tanggal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
