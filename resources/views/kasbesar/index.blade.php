@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE')
                            <h3 style="margin-top: 6px;">Kelola Transkasi</h3>
                        @else
                            <h3 style="margin-top: 6px;">Laporan Transkasi</h3>
                        @endif
                    </div>
                    <div class="ml-auto">
                        @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE')
                            <a href="{{ route('ui-kasbesar-create') }}" class="btn btn-primary">
                                <i class="fa fa-lw fa-plus"></i>
                            </a>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Rencana Keuangan</th>
                                <th scope="col">Saldo</th>
                                <th scope="col">Kredit</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Tanggal</th>
                                @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE')
                                    <th width="120"></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $dt)
                                <tr>
                                    <th scope="row">{{ $i }}</th>
                                    <td>{{ $dt->jenis_keuangan.' - '.$dt->subjenis_keuangan }}</td>
                                    <td>{{ "Rp " . number_format($dt->saldo,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($dt->kredit,2,',','.') }}</td>
                                    <td>{{ "Rp " . number_format($dt->debit,2,',','.') }}</td>
                                    <td>{{ $dt->keterangan }}</td>
                                    <td>{{ $dt->tanggal }}</td>
                                    @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE')
                                        <td>
                                            <a href="{{ route('ui-kasbesar-edit', $dt->id) }}" class="btn btn-warning">
                                                <i class="fa fa-lw fa-pencil-alt"></i>
                                            </a>

                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $dt->id }}">
                                                <i class="fa fa-lw fa-trash-alt"></i>
                                            </button>

                                            <div class="modal fade" id="deleteModal{{ $dt->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $dt->id }}" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="deleteModalLabel{{ $dt->id }}">Peringatan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    <div class="modal-body">
                                                        Data akan dihapus secara permanen, lanjutkan?
                                                    </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <a class="btn btn-danger" 
                                                                href="{{ route('ui-kasbesar-delete') }}"
                                                                onclick="event.preventDefault(); document.getElementById('id-form-{{ $dt->id }}').submit();"
                                                                >
                                                                Lanjutkan
                                                            </a>

                                                            <form id="id-form-{{ $dt->id }}" action="{{ route('ui-kasbesar-delete') }}" method="POST" style="display: block;">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{ $dt->id }}" />
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links() }}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
