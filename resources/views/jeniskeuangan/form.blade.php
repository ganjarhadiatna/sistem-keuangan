@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Jenis Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Jenis Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-jenis-keuangan-update') }} @else {{ route('ui-jenis-keuangan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <label for="inputJenisKeuangan">Jenis Keuangan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->jenis_keuangan }} @endif" class="form-control @error('jenis_keuangan') is-invalid @enderror" id="inputJenisKeuangan" name="jenis_keuangan" required>
                            @error('jenis_keuangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
