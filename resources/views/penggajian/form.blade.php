@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Penggajian</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Penggajian</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-penggajian-update') }} @else {{ route('ui-penggajian-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif
                        
                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputPegawai">Pegawai</label>
                                @if(isset($data))
                                    <select value="@if(isset($data)) {{ $data->pegawai_id }} @endif" class="form-control @error('pegawai_id') is-invalid @enderror" id="inputPegawai" name="pegawai_id" readonly>
                                @else
                                    <select value="@if(isset($data)) {{ $data->pegawai_id }} @endif" class="form-control @error('pegawai_id') is-invalid @enderror" id="inputPegawai" name="pegawai_id">
                                @endif
                                @foreach($pegawai as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->pegawai_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->nip . ' : ' . $dt->nama }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->nip . ' : ' . $dt->nama }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->nip . ' : ' . $dt->nama }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('pegawai_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="inputGaji">Gaji</label>
                            <input type="text" value="@if(isset($data)) {{ $data->gaji }} @endif" class="form-control @error('gaji') is-invalid @enderror" id="inputGaji" name="gaji" required>
                            @error('gaji')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTunjanganHariRaya">Tunjangan Hari Raya</label>
                            <input type="text" value="@if(isset($data)) {{ $data->tunjanganhariraya }} @endif" class="form-control @error('tunjanganhariraya') is-invalid @enderror" id="inputTunjanganHariRaya" name="tunjanganhariraya" required>
                            @error('tunjanganhariraya')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTunjanganKesehatan">Tunjangan Kesehatan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->tunjangankesehatan }} @endif" class="form-control @error('tunjangankesehatan') is-invalid @enderror" id="inputTunjanganKesehatan" name="tunjangankesehatan" required>
                            @error('tunjangankesehatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputPotongan">Potongan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->potongan }} @endif" class="form-control @error('potongan') is-invalid @enderror" id="inputPotongan" name="potongan" required>
                            @error('potongan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputTanggal">Tanggal</label>
                            <input type="date" value="@if(isset($data)) {{ $data->tanggal }} @endif" class="form-control @error('tanggal') is-invalid @enderror" id="inputTanggal" name="tanggal" required>
                            @error('tanggal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
