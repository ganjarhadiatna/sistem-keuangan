@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">

        @if(strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'ADMIN')
            <div class="card">
                <div class="card-header">
                    <div>Dashboard</div>
                </div>
                <div class="card-body">
                    <div style="padding-top: 15px; padding-bottom: 15px;">
                        <div class="wrapper">
                            <div style="width: 100%; margin-right: 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-user" style="color: red;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $admin['pegawai'] }}</h4>
                                            <p>Pegawai</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; margin-left: 15px;">
                                <div class="card">
                                    <div class="card-body wrapper">
                                        <div class="mr-auto">
                                            <i class="fa fa-4x fa-user" style="color: red;"></i>
                                        </div>
                                        <div class="ml-auto" style="text-align: right;">
                                            <h4>{{ $admin['pengguna'] }}</h4>
                                            <p>Pengguna</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(
            strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'DIREKTUR' || 
            strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'MANAGER FINANCE' || 
            strtoupper(Auth::user()->GetPegawaById(Auth::user()->id)->jabatan) == 'STAFF FINANCE'
        )
            <div class="card">
                <div class="card-header">
                    <div>KEUANGAN PT. SENTOSA MITRA SEMESTA</div>
                </div>
                <div class="card-body">
                    <form method="GET" action="{{ route('ui-monitoring-periode') }}">
                        <label for="inputPeriodeKeuangan">Periode Keuangan</label>
                        <div class="wrapper">
                            <div class="form-group" style="width: 100%; margin-right: 15px;">
                                <div class="form-group">
                                    <select value="@if(isset($periode)) {{ $periode }} @endif" class="form-control @error('periode_keuangan') is-invalid @enderror" id="inputPeriodeKeuangan" name="periode_keuangan">
                                    @foreach($data['periode'] as $dt)
                                        @if(isset($periode))
                                            @if($periode == $dt->tahun)
                                                <option value="{{ $dt->tahun }}" selected>{{ $dt->tahun }}</option>
                                            @else
                                                <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                            @endif
                                        @else
                                            <option value="{{ $dt->tahun }}">{{ $dt->tahun }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                                @error('periode_keuangan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">Proses</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card" style="margin-top: 20px;">
                <div class="card-header">
                    @if(isset($periode))
                        <div>KEUANGAN TAHUN {{ $periode }}</div>
                    @else
                        <div>PERIODE KEUANGAN BELUM DIPILIH</div>
                    @endif
                </div>
                <div class="card-body">
                    @if(isset($data['rencana_keuangan']))
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="60" scope="col">#</th>
                                    <th colspan="3" scope="col">Pos Keuangan</th>
                                    <th colspan="2" scope="col">Rencana Post Keuangan</th>
                                    <th colspan="1" scope="col">Pendapatan Periode {{ $periode_before }}</th>
                                    <th colspan="2" scope="col">Persentase</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php $i = 1 ?>
                            <?php $jumlah_keuangan = 0 ?>
                            <?php $jumlah_realisasi = 0 ?>
                            <?php $jumlah_persentase = 0 ?>
                            
                            @foreach($data['rencana_keuangan'] as $dt)
                                <?php $rencana_keuangan = ($dt->jumlah_keuangan + $dt->realisasi) ?>
                                <?php $realisasi = $dt->realisasi ?>
                                <?php $persentase = ($rencana_keuangan / $rencana_keuangan) * 100 ?>

                                <?php $jumlah_keuangan += $rencana_keuangan ?>
                                <?php $jumlah_realisasi += $realisasi ?>
                                <?php $jumlah_persentase += $persentase ?>
                            @endforeach

                            <?php $jumlah_pendapatan = 0 ?>

                            @foreach($data['pendapatan'] as $dt)
                                <?php $jumlah_pendapatan += $dt->jumlah_pendapatan ?>
                            @endforeach

                            <?php $idsub_before = 0 ?>
                            <?php $idsub_before2 = 0 ?>
                            <?php $idsub_before3 = 0 ?>

                            @foreach($data['rencana_keuangan'] as $dt)
                                <?php $rencana_keuangan = ($dt->jumlah_keuangan + $dt->realisasi) ?>
                                <?php $realisasi = $dt->realisasi ?>
                                <?php $persentase = ($rencana_keuangan / $jumlah_keuangan) * 100 ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    
                                    @if($i == 1)
                                        <td rowspan="{{ count($data['rencana_keuangan']) }}" style="vertical-align: middle;">{{ $periode }}</td>
                                    @endif

                                    @if($dt->total_sub > 1)
                                        @if($idsub_before != $dt->id_jenis_keuangan)
                                            <td rowspan="{{ $dt->total_sub }}" style="vertical-align: middle;">{{ $dt->jenis_keuangan }}</td>
                                        @endif
                                        <?php $idsub_before = $dt->id_jenis_keuangan ?>
                                    @else
                                        <td>{{ $dt->jenis_keuangan }}</td>
                                        <?php $idsub_before = 0 ?>
                                    @endif

                                    <td>{{ $dt->subjenis_keuangan }}</td>
                                    <td>{{ "Rp " . number_format($rencana_keuangan,2,',','.') }}</td>
                                    
                                    @if($dt->total_sub > 1)
                                        @if($idsub_before2 != $dt->id_jenis_keuangan)
                                            <td rowspan="{{ $dt->total_sub }}" style="vertical-align: middle;">{{ "Rp " . number_format(($dt->jumlah_sub + $dt->jumlah_realisasi),2,',','.') }}</td>
                                        @endif
                                        <?php $idsub_before2 = $dt->id_jenis_keuangan ?>
                                    @else
                                        <td>{{ "Rp " . number_format(($dt->jumlah_sub + $dt->jumlah_realisasi),2,',','.') }}</td>
                                        <?php $idsub_before2 = 0 ?>
                                    @endif

                                    @if($i == 1)
                                        <td rowspan="{{ count($data['rencana_keuangan']) }}" style="vertical-align: middle;">{{ "Rp " . number_format($jumlah_pendapatan,2,',','.') }}</td>
                                    @endif

                                    <td>{{ number_format($persentase, 2) }} %</td>

                                    @if($dt->total_sub > 1)
                                        @if($idsub_before3 != $dt->id_jenis_keuangan)
                                            <td rowspan="{{ $dt->total_sub }}" style="vertical-align: middle;">{{ number_format(((($dt->jumlah_sub + $dt->jumlah_realisasi) / $jumlah_keuangan) * 100), 2) }} %</td>
                                        @endif
                                        <?php $idsub_before3 = $dt->id_jenis_keuangan ?>
                                    @else
                                        <td>{{ number_format($persentase, 2) }} %</td>
                                        <?php $idsub_before3 = 0 ?>
                                    @endif
                                </tr>
                                <?php $i++ ?>
                            @endforeach

                            <tr>
                                <td>
                                <th colspan="4">Total</th>
                                <th colspan="1">{{ "Rp " . number_format($jumlah_keuangan,2,',','.') }}</th>
                                <th colspan="1">{{ "Rp " . number_format($jumlah_pendapatan,2,',','.') }}</th>
                                <th colspan="2"></th>
                            </tr>

                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        @endif

        </div>
    </div>
</div>
@endsection
