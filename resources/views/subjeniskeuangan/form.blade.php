@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header wrapper">
                    <div class="mr-auto">
                        @if(isset($data))
                            <h3 style="margin-top: 6px;">Ubah Subjenis Keuangan</h3>
                        @else
                            <h3 style="margin-top: 6px;">Tambah Subjenis Keuangan</h3>
                        @endif
                    </div>
                    <div class="ml-auto"></div>
                </div>

                <div class="card-body">
                    <form method="POST" action="@if(isset($data)) {{ route('ui-subjenis-keuangan-update') }} @else {{ route('ui-subjenis-keuangan-save') }} @endif">
                        @csrf
                        
                        @if(isset($data))
                            <input type="hidden" name="id" value="{{ $data->id }}">
                        @endif

                        <div class="form-group">
                            <div class="form-group">
                                <label for="inputJenisKeuangan">Jenis Keuangan</label>
                                <select value="@if(isset($data)) {{ $data->jeniskeuangan_id }} @endif" class="form-control @error('jeniskeuangan_id') is-invalid @enderror" id="inputJenisKeuangan" name="jeniskeuangan_id">
                                @foreach($jenis as $dt)
                                    @if(isset($data))
                                        @if($dt->id == $data->jeniskeuangan_id)
                                            <option value="{{ $dt->id }}" selected>{{ $dt->jenis_keuangan }}</option>
                                        @else
                                            <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $dt->id }}">{{ $dt->jenis_keuangan }}</option>
                                    @endif
                                @endforeach
                                </select>
                            </div>
                            @error('jeniskeuangan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inputSubjenisKeuangan">Subjenis Keuangan</label>
                            <input type="text" value="@if(isset($data)) {{ $data->subjenis_keuangan }} @endif" class="form-control @error('subjenis_keuangan') is-invalid @enderror" id="inputSubjenisKeuangan" name="subjenis_keuangan" required>
                            @error('subjenis_keuangan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
